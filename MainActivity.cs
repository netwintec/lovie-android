﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Json;

using Android.App;
using Android.Widget;
using Android.OS;
using Android.Content;
using Android.Content.PM;
using Android.Runtime;
using Android.Views;
using Android.Views.Animations;
using Android.Util;

using Xamarin.Facebook;
using Xamarin.Facebook.Login;
using Xamarin.Facebook.Login.Widget;
using Xamarin.Facebook.AppEvents;
using RestSharp;
using Newtonsoft.Json.Linq;
using Gcm.Client;


namespace Lovie
{
	[Activity (ScreenOrientation = ScreenOrientation.Portrait)]
	public class MainActivity : Activity
	{

		static string TXT_EULA = "Termini e condizioni di uso della App di Lovie\n\n1. Uso dell’App Lovie\nScaricando, installando e utilizzando l’APP Lovie l’utente accetta i seguenti termini e condizioni, nonché qualsiasi modifica che potrà essere apportata.\nAl fine di accedere ai servizi e usare l’APP Lovie l’utente potrà essere tenuto a fornire i dati occorrenti per la propria identificazione o,comunque, i dati occorrenti per consentire l’uso dell’APP Lovie e dei servizi ad essa associati. Lovie non potrà essere ritenuta responsabile per qualsiasi perdita o danno derivante dalla comunicazione di dati e di informazioni non veritiere.\nL‘APP Lovie potrà visualizzare annunci pubblicitari e promozioni sia di Lovie stessa, sia di società ad essa collegate, sia di terzi.\nL’utente accetta che Lovie possa inserire tale pubblicità e che non sia ritenuta responsabile per qualsiasi perdita o danno di alcun tipo subito dall’utente a seguito della presenza di tali inserzionisti o dei conseguenti rapporti dell’utente con tali inserzionisti.\nL’APP Lovie potrà fornire link a siti o risorse web. L’utente riconosce e accetta quindi che Lovie non è responsabile per la disponibilità di tali risorse o siti esterni e che non avalla e non è responsabile per i relativi contenuti, la pubblicità, i prodotti o altri materiali presenti o disponibili su tali siti o risorse.\nL’utente e Lovie potranno interrompere l’uso dell’APP Lovie in qualsiasi momento. Lovie si riserva il diritto di modificare, sospendere o interrompere i servizi forniti attraverso l’APP Lovie o parte di essi, in qualsiasi momento e con o senza preavviso. \nL’utente accetta che Lovie non sarà ritenuta responsabile nei propri confronti o di qualsiasi terza parte per le predette modifiche, le sospensioni o le interruzioni.\nL’APP può essere scaricata e utilizzata solo da soggetti di maggiore età: l’utente pertanto dovrà avere un età di almeno 18 anni.\n\n2. Servizi fruibili tramite l’APP di Lovie\nLovie rende fruibili tramite la propria APP, i servizi di chat con utenti Facebook e Whatsapp.\nL’utente ha l’obbligo di utilizzare il servizio nel rispetto delle leggi, dei regolamenti tempo per tempo vigenti, astenendosi dall’inviare messaggio di contenuto illegale o illecito.\nL’utente, in particolare e senza che ciò implichi alcuna deroga a quanto sopra, prende atto e accetta che:\na) i servizi devono essere utilizzati nel rispetto dei principi di buona fede e correttezza;\nb) fatto divieto di usufruire dei servizi  per scopi o attività illecite o fraudolente;\nc) Lovie è unicamente responsabile della fornitura dei servizi di accesso.\nPer poter utilizzare i servizi in piena sicurezza dall’APP Lovie l’utente dovrà registrarsi utilizzando un proprio codice di sicurezza secondo le modalità e le indicazioni fornite direttamente durante la procedura di download ovvero specificate sul sito www.lovie.it.\nL’utente è l’unico responsabile del corretto e personale uso dell’APP e della relativa password, della sua custodia e dell’inaccessibilità a terzi, nonché della custodia e del corretto uso del terminale.\nL’utente è pertanto tenuto a custodire il terminale e la proprio password con particolare attenzione, di non cedere o dare in uso a terzi il proprio terminale, di non comunicare o trascrivere la propria password su alcun supporto, nonché di tenerlo separato dal terminale.\nIn caso di fondato sospetto che terzi siano venuti a conoscenza della propria Password, è necessario provvedere all’immediata modifica dello stesso.\nIn caso di dismissione o cessione a terzi del proprio terminale, l’utente dovrà rimuovere sempre l’APP dal terminale.\nL’utente infine prende atto e accetta che, al fine di tutelare l’integrità delle reti, la sicurezza del servizio ed evitare frodi o utilizzi anomali dei servizi, Lovie si riserva la facoltà di adottare appropriati interventi, ivi incluse limitazioni o sospensioni della fruizione dei SMD stessi.\n\n3. Norme sulla privacy\nInstallando l’APP Lovie l’utente dichiara e accetta che Lovie possa accedere, conservare e trattare i dati personali dell’utente e qualsiasi contenuto associato all’uso dell’APP, se richiesto per legge o se tali attività siano necessarie per l’uso dell’APP Lovie ovvero dei servizi forniti attraverso l’APP, nel pieno rispetto dei principi dettati dal D. lgs. 196/03 “Codice in materia di protezione dei dati personali”\n(“Codice”). L’utente riconosce che l’uso dell’APP Lovie può prevedere la trasmissione di dati attraverso reti di comunicazione elettronica.\nI dati trattati sono quelli necessari al corretto funzionamento dell’APP e dei relativi servizi, nello specifico i dati obbligatori richiesti sono: Nome, Cognome, Telefono e Immagine del profilo. Questi dati possono essere presi anche da Facebook qualora l’utente si registri, dando il consenso, con il proprio utente e password di Facebook. Con il consenso dell’utente Lovie potrà inoltre, inviare comunicazioni aventi ad oggetto offerte e/o promozioni di Lovie e/o di partner di Lovie  con le modalità e le forme previste dall’art. 130 del Codice.\nLovie non potrà essere ritenuta responsabile per qualsiasi perdita o danno derivante dalla comunicazione di tali dati.\nL’utente può venire a conoscenza a quindi trattare dati di terzi (foto, video, messaggi, esperienze, eventi, incontri, preferenze etc..): tali dati andranno trattati nel rispetto del d. lgs. 196/2003 e delle leggi civili e penali. L’utente è il titolare di tale trattamento e l’unico ed esclusivo responsabile.\nLovie non conosce i contenuti delle comunicazioni o dei messaggi scambiate dagli utenti, può in caso di richiesta da parte delle autorità fornire copia delle specifiche comunicazioni che sono conservate sui nostri sistemi per un tempo illimitato.";


		ICallbackManager callbackManager;
		LoginButton loginButton;

		EditText phone,code,prefix;

		Button Verify;
		View PTrasparent,NTrasparent;
		ImageView loadImage,avanti;

		bool numberInsert=false;

		Animation rotateAboutCenterAnimation;

		string tokenAppoggio;
		string tokenfbAppoggio;

		public ISharedPreferences prefs;
		public string PushToken;

		public bool isHome;
		public bool isChat;
		public string ChatKey;
		public bool isNothing;
		public bool isStopped;

		bool notification;
		string name,conv_id;
		bool Registrati = false;

		string Nome, Cognome;
		EditText NomeText, CognomeText;

		protected override void OnCreate (Bundle savedInstanceState)
		{
			base.OnCreate (savedInstanceState);

			PushToken = "";

			GcmClient.CheckDevice(this);
			GcmClient.CheckManifest(this);
			GcmClient.Register(this, GcmBroadcastReceiver.SENDER_IDS);

			FacebookSdk.SdkInitialize (Application);

			prefs = Application.Context.GetSharedPreferences ("RoccoGiocattoli", FileCreationMode.Private);

			MainActivity.Instance = this;

			isHome = false;
			isChat = false;
			ChatKey = "";
			isNothing = true;

			notification = Intent.GetBooleanExtra ("Notification", false);
			name = Intent.GetStringExtra ("name");
			conv_id = Intent.GetStringExtra ("key");

			// Set our view from the "main" layout resource
			SetContentView (Resource.Layout.Main);
			ActionBar.Hide ();

			callbackManager = CallbackManagerFactory.Create ();

			FindViewById<RelativeLayout> (Resource.Id.BottomViewCheck).Visibility = ViewStates.Gone;
			FindViewById<RelativeLayout> (Resource.Id.BottomLoadView).Visibility = ViewStates.Gone;
			FindViewById<RelativeLayout>(Resource.Id.BottomViewFB).Visibility = ViewStates.Visible;

			NomeText = FindViewById<EditText>(Resource.Id.NomeText);
			CognomeText = FindViewById<EditText>(Resource.Id.CognomeText);

			phone = FindViewById<EditText> (Resource.Id.NumberText);
			prefix = FindViewById<EditText> (Resource.Id.PrefixText);
			code = FindViewById<EditText> (Resource.Id.CodeText);
			loadImage = FindViewById<ImageView> (Resource.Id.loadImage);
			avanti = FindViewById<ImageView>(Resource.Id.Avanti);

			PTrasparent = FindViewById<View> (Resource.Id.PhoneView);
			NTrasparent = FindViewById<View> (Resource.Id.NumberView);


			avanti.Click += delegate {
				
				if (!string.IsNullOrWhiteSpace(NomeText.Text) && !string.IsNullOrWhiteSpace(CognomeText.Text)) {

					Nome = NomeText.Text;
					Cognome = CognomeText.Text;

					FindViewById<RelativeLayout>(Resource.Id.BottomLoadView).Visibility = ViewStates.Gone;
					FindViewById<RelativeLayout>(Resource.Id.BottomViewFB).Visibility = ViewStates.Gone;
					FindViewById<RelativeLayout>(Resource.Id.BottomViewCheck).Visibility = ViewStates.Visible;

					NTrasparent.Alpha = 0;
					Registrati = true;
				}

			};

			Verify = FindViewById<Button> (Resource.Id.ButtonVerify);
			Verify.Click += delegate {



				if(!numberInsert && !string.IsNullOrWhiteSpace(phone.Text) && !string.IsNullOrWhiteSpace(prefix.Text)){

					bool numberValid = true;

					//string sub = phone.Text.Substring(1);

					try{
						
						Int64 numValid = Int64.Parse(phone.Text);

					}catch(Exception e){
						Console.WriteLine("3 "+e.ToString());
						numberValid = false;

					}

					/*if(phone.Text.Count() != 10){
						Console.WriteLine("1");
						numberValid = false;
					}*/

					if(!prefix.Text.Contains("+") && phone.Text.Count()<=1){
						Console.WriteLine("2");
						numberValid = false;
					}

					if(numberValid){

						//********** VERIFICA NUMERO ******* 
						if (!Registrati)
						{
							// DEVELOPMENT 
							//var client = new RestClient("http://location.keepup.pro:3000/");
							//DISTRIBUTION
							var client = new RestClient("http://api.netwintec.com:3000/");


							var requestN4U = new RestRequest("api/login/get_phone_token", Method.POST);
							requestN4U.AddHeader("content-type", "application/json");
							requestN4U.AddHeader("lovie-token", tokenAppoggio);

							JObject oJsonObject = new JObject();

							oJsonObject.Add("phone", prefix.Text + phone.Text);

							requestN4U.AddParameter("application/json; charset=utf-8", oJsonObject, ParameterType.RequestBody);


							IRestResponse response = client.Execute(requestN4U);

							Console.WriteLine("RESPONSE:" + response.Content + "|" + response.StatusCode);

							if (response.StatusCode == System.Net.HttpStatusCode.OK)
							{

								PTrasparent.Alpha = 0;
								NTrasparent.Alpha = 1;
								prefix.Enabled = false;
								phone.Enabled = false;
								code.Enabled = true;
								numberInsert = true;
								Verify.Text = "Verifica il codice";
							}
							else {

								phone.Text = "";

								AlertDialog.Builder alert = new AlertDialog.Builder(this);
								alert.SetTitle("Errore ");
								alert.SetMessage("Numero male inserito oppure già presente.");
								alert.SetPositiveButton("Riprova", (senderAlert, args) => {} );
								alert.Show();

							}

						}
						else
						{
							bool errore = false;

							// DEVELOPMENT 
							//var client = new RestClient("http://location.keepup.pro:3000/");
							//DISTRIBUTION
							var client = new RestClient("http://api.netwintec.com:3000/");


							var requestN4U = new RestRequest("api/registration", Method.POST);
							requestN4U.AddHeader("content-type", "application/json");

							JObject oJsonObject = new JObject();


							oJsonObject.Add("name", Nome + " " + Cognome);
							oJsonObject.Add("device_token", PushToken);
							oJsonObject.Add("device_type", "android");
							oJsonObject.Add("phone", prefix.Text + phone.Text);
							oJsonObject.Add("device_uuid", Android.OS.Build.Serial);

							requestN4U.AddParameter("application/json; charset=utf-8", oJsonObject, ParameterType.RequestBody);


							IRestResponse response = client.Execute(requestN4U);

							Console.WriteLine("RESPONSE REG:" + response.Content + "|" + response.StatusCode);

							if (response.StatusCode == System.Net.HttpStatusCode.OK)
							{
								JsonValue json = JsonValue.Parse(response.Content);
								string token = json["token"];
								int verified = json["verified"];

								if (verified == 0)
								{
									PTrasparent.Alpha = 0;
									NTrasparent.Alpha = 1;
									prefix.Enabled = false;
									phone.Enabled = false;
									code.Enabled = true;
									numberInsert = true;
									Verify.Text = "Verifica il codice";

									tokenAppoggio = token;
								}
								else
								{
									errore = true;

								}
							}
							else
							{

								errore = true;

							}


							if (errore) {
								// DEVELOPMENT 
								//var client = new RestClient("http://location.keepup.pro:3000/");
								//DISTRIBUTION
								var client2 = new RestClient("http://api.netwintec.com:3000/");


								var request2N4U = new RestRequest("api/login/standard ", Method.POST);
								request2N4U.AddHeader("content-type", "application/json");

								JObject oJsonObject2 = new JObject();


								oJsonObject2.Add("name", Nome + " " + Cognome);
								oJsonObject2.Add("device_token", PushToken);
								oJsonObject2.Add("device_type", "android");
								oJsonObject2.Add("phone", prefix.Text + phone.Text);
								oJsonObject2.Add("device_uuid", Android.OS.Build.Serial);

								request2N4U.AddParameter("application/json; charset=utf-8", oJsonObject2, ParameterType.RequestBody);


								IRestResponse response2 = client2.Execute(request2N4U);

								Console.WriteLine("RESPONSE LOG:" + response2.Content + "|" + response2.StatusCode);

								if (response2.StatusCode == System.Net.HttpStatusCode.OK)
								{
									JsonValue json2 = JsonValue.Parse(response2.Content);
									string token2 = json2["token"];
									int verified2 = json2["verified"];

									if (verified2 == 0)
									{
										PTrasparent.Alpha = 0;
										NTrasparent.Alpha = 1;
										prefix.Enabled = false;
										phone.Enabled = false;
										code.Enabled = true;
										numberInsert = true;
										Verify.Text = "Verifica il codice";

										tokenAppoggio = token2;
									}
									else
									{


										phone.Text = "";

										AlertDialog.Builder alert = new AlertDialog.Builder(this);
										alert.SetTitle("Errore ");
										alert.SetMessage("Numero male inserito oppure già presente.");
										alert.SetPositiveButton("Riprova", (senderAlert, args) => { });
										alert.Show();

									}
								}
								else
								{

									phone.Text = "";

									AlertDialog.Builder alert = new AlertDialog.Builder(this);
									alert.SetTitle("Errore ");
									alert.SetMessage("Numero male inserito oppure già presente.");
									alert.SetPositiveButton("Riprova", (senderAlert, args) => { });
									alert.Show();

								}
							}
						}

					}

				}

				if(numberInsert && !string.IsNullOrWhiteSpace(code.Text)){

					//********** VERIFICA CODICE *******

					//var client = new RestClient("http://location.keepup.pro:3000/");
					//DISTRIBUTION
					var client = new RestClient("http://api.netwintec.com:3000/");


					var requestN4U = new RestRequest("api/login/verify_phone_token", Method.POST);
					requestN4U.AddHeader("content-type", "application/json");
					requestN4U.AddHeader("lovie-token",tokenAppoggio);

					JObject oJsonObject = new JObject();

					oJsonObject.Add("phone_token", code.Text);

					requestN4U.AddParameter("application/json; charset=utf-8", oJsonObject, ParameterType.RequestBody);


					IRestResponse response = client.Execute(requestN4U);

					Console.WriteLine("RESPONSE:"+response.Content+"|"+response.StatusCode);

					if(response.StatusCode == System.Net.HttpStatusCode.OK){

						var prefEditor = prefs.Edit();
						prefEditor.PutString("TokenN4U",tokenAppoggio);
						if(!Registrati)
							prefEditor.PutString("TokenFB",tokenfbAppoggio);
						prefEditor.Commit();

						var intent = new Intent (this, typeof(HomePageActivity));
						this.StartActivity (intent);
					}
					else{

						code.Text="";

						AlertDialog.Builder alert = new AlertDialog.Builder (this);
						alert.SetTitle("Errore ");
						alert.SetMessage ("Codice non corrispondente Riprovare oppure riavviare l'applicazione per cambiare numero.");
						alert.SetPositiveButton ("Riprova", (senderAlert, args) => {} );
						alert.Show();

					}


				}
			};

			var loginCallback = new FacebookCallback<LoginResult> {

				HandleSuccess = loginResult => {
					//login
					Console.WriteLine ("Loggato con successo");
					Console.WriteLine(AccessToken.CurrentAccessToken.Token);

					//DEVELOPMENT
					//var client = new RestClient("http://location.keepup.pro:3000/");
					//DISTRIBUTION
					var client = new RestClient("http://api.netwintec.com:3000/");


					var requestN4U = new RestRequest("api/login/facebook", Method.POST);
					requestN4U.AddHeader("content-type", "application/json");

					JObject oJsonObject = new JObject();

					oJsonObject.Add("access_token", AccessToken.CurrentAccessToken.Token);
					oJsonObject.Add("device_token", PushToken);
					oJsonObject.Add("device_type", "android" );
					oJsonObject.Add("device_uuid", Android.OS.Build.Serial );

					requestN4U.AddParameter("application/json; charset=utf-8", oJsonObject, ParameterType.RequestBody);


					client.ExecuteAsync(requestN4U,(s,e) => {

						Console.WriteLine("RESPONSE"+s.Content+"|"+s.StatusCode);



						if (s.StatusCode == System.Net.HttpStatusCode.OK)
						{

							RunOnUiThread(() => {

								JsonValue json = JsonValue.Parse(s.Content);
								//Console.WriteLine (response.Content);
								string token = json["token"];
								int verified = json["verified"];

								Console.WriteLine(token +"|"+verified);

								if(verified == 0){

									FindViewById<RelativeLayout> (Resource.Id.BottomLoadView).Visibility = ViewStates.Gone;
									FindViewById<RelativeLayout> (Resource.Id.BottomViewFB).Visibility = ViewStates.Gone;
									FindViewById<RelativeLayout> (Resource.Id.BottomViewCheck).Visibility = ViewStates.Visible;

									tokenAppoggio =  token;
									tokenfbAppoggio = AccessToken.CurrentAccessToken.Token;

									NTrasparent.Alpha = 0;
									//code.Enabled = false;
								}
								else{

									var prefEditor = prefs.Edit();
									prefEditor.PutString("TokenN4U",token);
									prefEditor.PutString("TokenFB",AccessToken.CurrentAccessToken.Token);
									prefEditor.Commit();

									var intent = new Intent (this, typeof(HomePageActivity));
									this.StartActivity (intent);

								}

								
							});

						}
						else{

							Console.WriteLine("1a:"+(s.StatusCode!=0));
							RunOnUiThread(() => {

								LoginManager.Instance.LogOut();

								FindViewById<RelativeLayout> (Resource.Id.BottomLoadView).Visibility = ViewStates.Gone;
								FindViewById<RelativeLayout> (Resource.Id.BottomViewFB).Visibility = ViewStates.Visible;
								FindViewById<RelativeLayout> (Resource.Id.BottomViewCheck).Visibility = ViewStates.Gone;
												

								AlertDialog.Builder alert = new AlertDialog.Builder (this);
								alert.SetTitle("Errore Login");
								alert.SetMessage ("Login non Riuscito riprovare più tardi");
								alert.SetPositiveButton ("OK", (senderAlert, args) => {} );
								alert.Show();
								Console.WriteLine("1b"); 
							});

						}
					});

					FindViewById<RelativeLayout> (Resource.Id.BottomLoadView).Visibility = ViewStates.Visible;
					FindViewById<RelativeLayout> (Resource.Id.BottomViewFB).Visibility = ViewStates.Gone;
					FindViewById<RelativeLayout> (Resource.Id.BottomViewCheck).Visibility = ViewStates.Gone;

					rotateAboutCenterAnimation = AnimationUtils.LoadAnimation(this,Resource.Animation.RotateAnimation);
					loadImage.StartAnimation(rotateAboutCenterAnimation);


				},
				HandleCancel = () => {
					Console.WriteLine ("Condizioni non accettate");
				},
				HandleError = loginError => {
					AlertDialog.Builder alert = new AlertDialog.Builder (this);
					alert.SetTitle("Errore Login con Facebook");
					alert.SetMessage ("Login non Riuscito riprovare più tardi");
					alert.SetPositiveButton ("OK", (senderAlert, args) => {} );
					alert.Show();
				}
			};

			LoginManager.Instance.RegisterCallback (callbackManager, loginCallback);

			loginButton = (LoginButton) FindViewById<Button> (Resource.Id.LoginFb);

			loginButton.SetReadPermissions("public_profile","user_birthday","email");

			string t = "eyJhbGciOiJIUzI1NiJ9.dXNlcjo6ZmFjZWJvb2s6OjE0Njk4ODQyODE.dM-0RRZixM--m__vfQ-CZhQ9DP7xWH2LzlxZjmSrQ-o";
			string tFB = "CAACEdEose0cBAFtzzsZAKcYZA6dpPp23VbPZCJqxTEOne63whJXpczkaExl5CoOi0Hf7Y7S15LFBdwbfZBjNVVwlxyDwBWLsVYSKA9I7JZBV2BZBuqhol5O2zUOt4Btt7tZAuR944bGlzdNuR9icVfVPAzzRiKKfxjz4wq94A0u9ZCigoOWiGmRo6K4hyE2T7FOQ16THbPK6sAZDZD";
			//var prefEditor2 = prefs.Edit();
			//prefEditor2.PutString("TokenN4U",t);
			//prefEditor2.PutString("TokenFB",tFB);
			//prefEditor2.Commit();

			Console.WriteLine ("ID:"+Android.OS.Build.Serial);

			if(prefs.GetString("TokenN4U","")!=""){

				//DEVELOPMENT
				//var client = new RestClient("http://location.keepup.pro:3000/");
				//DISTRIBUTION
				var client = new RestClient("http://api.netwintec.com:3000/");


				var requestN4U = new RestRequest("api/user/"+prefs.GetString("TokenN4U",""), Method.GET);
				requestN4U.AddHeader("content-type", "application/json");

				var response = client.Execute (requestN4U);
				Console.WriteLine ("R:" + response.Content + "|" + response.StatusCode);

				if (response.StatusCode == System.Net.HttpStatusCode.OK) {
					Console.WriteLine ("Token:" + prefs.GetString ("TokenN4U", ""));
					var intent = new Intent (this, typeof(HomePageActivity));

					if (notification) {
						intent.PutExtra ("name", name);
						intent.PutExtra ("key", conv_id);
						intent.PutExtra ("Notification", true);
					}
					
					this.StartActivity (intent);
				} else {

					var prefEditor = prefs.Edit();
					prefEditor.PutString("TokenN4U", "");
					prefEditor.PutString("TokenFB", "");
					prefEditor.Commit();

					LoginManager.Instance.LogOut ();

				}
			}
			else {

				var prefEditor = prefs.Edit();
				prefEditor.PutString("TokenN4U", "");
				prefEditor.PutString("TokenFB", "");
				prefEditor.Commit();

				LoginManager.Instance.LogOut ();

			}


			/* EULA PART */

			TextView EulaText = FindViewById<TextView>(Resource.Id.EulaText);
			RelativeLayout EulaView = FindViewById<RelativeLayout>(Resource.Id.EulaView);
			Button ChiudiButton = FindViewById<Button>(Resource.Id.ChiudiButton);
			Button OpenEula = FindViewById<Button>(Resource.Id.OpenEula);
			ScrollView sv = FindViewById<ScrollView>(Resource.Id.scrollView1);

			EulaText.Text = TXT_EULA;
			EulaView.Visibility = ViewStates.Gone;

			ChiudiButton.Click += delegate {

				EulaView.Visibility = ViewStates.Gone;

			};

			OpenEula.Click += delegate
			{
				sv.ScrollTo(0, 0);
				EulaView.Visibility = ViewStates.Visible;

			};

				
		}

		public static MainActivity Instance {get;private set;}

		protected override void OnActivityResult (int requestCode, Result resultCode, Intent data)
		{

			Console.WriteLine ("aa");
			base.OnActivityResult (requestCode, resultCode, data);

			callbackManager.OnActivityResult (requestCode, (int)resultCode, data);
		}

		protected override void OnDestroy ()
		{
			base.OnDestroy ();
		}

		protected override void OnPause ()
		{
			base.OnPause ();
			AppEventsLogger.DeactivateApp (this);
		}

		protected override void OnResume ()
		{
			base.OnResume ();
			AppEventsLogger.ActivateApp (this);
		}

	}

	class FacebookCallback<TResult> : Java.Lang.Object, IFacebookCallback where TResult : Java.Lang.Object
	{
		public Action HandleCancel { get; set; }
		public Action<FacebookException> HandleError { get; set; }
		public Action<TResult> HandleSuccess { get; set; }

		public void OnCancel ()
		{
			var c = HandleCancel;
			if (c != null)
				c ();
		}

		public void OnError (FacebookException error)
		{
			var c = HandleError;
			if (c != null)
				c (error);
		}

		public void OnSuccess (Java.Lang.Object result)
		{
			var c = HandleSuccess;
			if (c != null)
				c (result.JavaCast<TResult> ());
		}


	}
	class CustomProfileTracker : ProfileTracker
	{
		public delegate void CurrentProfileChangedDelegate (Profile oldProfile, Profile currentProfile);

		public CurrentProfileChangedDelegate HandleCurrentProfileChanged { get; set; }

		protected override void OnCurrentProfileChanged (Profile oldProfile, Profile currentProfile)
		{
			var p = HandleCurrentProfileChanged;
			if (p != null)
				p (oldProfile, currentProfile);
		}
	}
}


