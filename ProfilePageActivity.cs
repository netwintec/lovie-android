﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Json;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Content.PM;
using Android.Graphics;

using Xamarin.InAppBilling;
using Xamarin.InAppBilling.Utilities;
using RestSharp;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Xamarin.Facebook.Login;

namespace Lovie
{
	[Activity (Label = "ProfilePageActivity",Theme = "@style/AppTheme",ScreenOrientation = ScreenOrientation.Portrait)]			
	public class ProfilePageActivity : BaseActivity
	{

		ImageView menuIcon;
		ImageView backIcon;
		TextView Name,Telefono;
		TextView MessaggiRimasti,MessaggiInviati;
		RelativeLayout Product1,Product2,Product3,Product4,Product5;
		InAppBillingServiceConnection _serviceConnection;
		ISharedPreferences prefs;

		TextView Price1,Price2,Price3,Price4,Price5;

		IList<Product> Prodotti;

		protected override void OnCreate (Bundle savedInstanceState)
		{
			base.OnCreate (savedInstanceState);

			prefs = MainActivity.Instance.prefs;

			Typeface RobotoR = Typeface.CreateFromAsset(Assets,"fonts/Roboto_Regular.ttf");

			menuIcon = (ImageView) getToolbar().FindViewById(Resource.Id.menu_icon);
			backIcon = (ImageView) getToolbar().FindViewById(Resource.Id.back_icon);

			setActionBarIcon(Resource.Drawable.logout,Resource.Drawable.back);

			menuIcon.Visibility = ViewStates.Visible;
			backIcon.Visibility = ViewStates.Visible;

			MainActivity.Instance.isHome = false;
			MainActivity.Instance.isChat = false;
			MainActivity.Instance.ChatKey = "";
			MainActivity.Instance.isNothing = true;
			//mDrawerLayout.SetDrawerShadow(R.drawable.drawer_shadow,GravityCompat.START);

			backIcon.Click += delegate {
				
				var intent = new Intent (this, typeof(HomePageActivity));
				this.StartActivity (intent);
				Finish();

			};

			menuIcon.Click += delegate {

				/*var intent = new Intent (this, typeof(HomePageActivity));
				this.StartActivity (intent);
				//Finish();*/

				AlertDialog.Builder alert = new AlertDialog.Builder (this);
				alert.SetTitle("Logout");
				alert.SetMessage ("Vuoi veramente uscire?");
				alert.SetCancelable(false);
				alert.SetPositiveButton ("Logout", (senderAlert, args) => {
					//var client = new RestClient("http://location.keepup.pro:3000/");
					//DISTRIBUTION
					var client = new RestClient("http://api.netwintec.com:3000/");

					ISharedPreferences prefs = MainActivity.Instance.prefs;
					string token = prefs.GetString ("TokenN4U", "");

					if (token == "")
					{

						// ERRORE
						return;

					}

					var requestN4U = new RestRequest("api/logout", Method.POST);
					requestN4U.AddHeader("content-type", "application/json");
					requestN4U.AddHeader("lovie-token", token);

					JObject oJsonObject = new JObject();

					oJsonObject.Add("device_uuid", Android.OS.Build.Serial);

					requestN4U.AddParameter("application/json; charset=utf-8", oJsonObject, ParameterType.RequestBody);

					IRestResponse response = client.Execute(requestN4U);

					if (response.StatusCode == System.Net.HttpStatusCode.OK)
					{
						var prefEditor = prefs.Edit();
						prefEditor.PutString("TokenN4U","");
						prefEditor.PutString("TokenFB","");
						prefEditor.Commit();

						LoginManager.Instance.LogOut();

						var intent = new Intent (this, typeof(MainActivity));
						this.StartActivity (intent);
						Finish();
					}
					else {
						Toast.MakeText (ApplicationContext, "Errore nel logout provare più tardi", ToastLength.Long).Show();
					}
				});
				alert.SetNegativeButton("Annulla",(senderAlert, args) => {} );
				alert.Show();

			};

			Name = FindViewById<TextView> (Resource.Id.Nome);
			Name.Typeface = RobotoR;
			Telefono = FindViewById<TextView> (Resource.Id.Telefono);
			Telefono.Typeface = RobotoR;
			FindViewById<TextView> (Resource.Id.acquista).Typeface = RobotoR;

			FindViewById<TextView> (Resource.Id.NameProduct1).Typeface = RobotoR;
			Price1 = FindViewById<TextView> (Resource.Id.PriceProduct1);
			Price1.Typeface = RobotoR;

			FindViewById<TextView> (Resource.Id.NameProduct2).Typeface = RobotoR;
			Price2 = FindViewById<TextView> (Resource.Id.PriceProduct2);
			Price2.Typeface = RobotoR;

			FindViewById<TextView> (Resource.Id.NameProduct3).Typeface = RobotoR;
			Price3 = FindViewById<TextView> (Resource.Id.PriceProduct3);
			Price3.Typeface = RobotoR;

			FindViewById<TextView> (Resource.Id.NameProduct4).Typeface = RobotoR;
			Price4 = FindViewById<TextView> (Resource.Id.PriceProduct4);
			Price4.Typeface = RobotoR;

			FindViewById<TextView> (Resource.Id.NameProduct5).Typeface = RobotoR;
			Price5 = FindViewById<TextView> (Resource.Id.PriceProduct5);
			Price5.Typeface = RobotoR;

			FindViewById<TextView> (Resource.Id.MessageSendLabel).Typeface = RobotoR;
			MessaggiInviati = FindViewById<TextView> (Resource.Id.MessageSendNumber);
			MessaggiInviati.Typeface = RobotoR;

			FindViewById<TextView> (Resource.Id.MessageRemainLabel).Typeface = RobotoR;
			MessaggiRimasti = FindViewById<TextView> (Resource.Id.MessageRemainNumber);
			MessaggiRimasti.Typeface = RobotoR;

			bool result =GetData ();

			if (!result) {

				var intent = new Intent (this, typeof(HomePageActivity));
				this.StartActivity (intent);
				Finish ();

			}
			Product1 = FindViewById<RelativeLayout> (Resource.Id.Product1);
			Product2 = FindViewById<RelativeLayout> (Resource.Id.Product2);
			Product3 = FindViewById<RelativeLayout> (Resource.Id.Product3);
			Product4 = FindViewById<RelativeLayout> (Resource.Id.Product4);
			Product5 = FindViewById<RelativeLayout> (Resource.Id.Product5);

			Product1.Enabled = false;
			Product2.Enabled = false;
			Product3.Enabled = false;
			Product4.Enabled = false;
			Product5.Enabled = false;

			Product1.SetBackgroundColor (Color.LightGray);
			Product2.SetBackgroundColor (Color.LightGray);
			Product3.SetBackgroundColor (Color.LightGray);
			Product4.SetBackgroundColor (Color.LightGray);
			Product5.SetBackgroundColor (Color.LightGray);

			Console.WriteLine (Product1.Width+"  "+Product1.Height);

			Product1.Click += delegate {
				PurchaseProduct(0);
			};

			Product2.Click += delegate {
				PurchaseProduct(1);
			};

			Product3.Click += delegate {
				PurchaseProduct(2);
			};

			Product4.Click += delegate {
				PurchaseProduct(3);
			};

			Product5.Click += delegate {
				PurchaseProduct(4);
			};

			StartSetup ();

		}

		public override void OnBackPressed ()
		{
			var intent = new Intent (this, typeof(HomePageActivity));
			this.StartActivity (intent);
			Finish();
		}

		protected override int getLayoutResource() {
			return Resource.Layout.ProfiloPage;
		}

		public bool GetData(){

			//DEVELOPMENT
			//var client = new RestClient("http://location.keepup.pro:3000/");
			//DISTRIBUTION
			var client = new RestClient("http://api.netwintec.com:3000/");


			var requestN4U = new RestRequest("api/user/"+prefs.GetString("TokenN4U",""), Method.GET);
			requestN4U.AddHeader("content-type", "application/json");

			var response = client.Execute (requestN4U);

			if (response.StatusCode == System.Net.HttpStatusCode.OK) {

				JsonValue json = JsonValue.Parse(response.Content);

				Name.Text = json ["name"];
				Telefono.Text = json["phone"];
				try{
					int value = json ["credit"];
					MessaggiRimasti.Text = value.ToString();
				}catch(Exception e){
					MessaggiRimasti.Text ="0";
				}
				try{
					int value2 = json["message_count"];
					MessaggiInviati.Text = value2.ToString();
				}catch(Exception e){
					MessaggiInviati.Text = "0";
				}
				return true;

			}

			return false;
		}

		public void StartSetup ()
		{
			/*
				APP KEY: 
				MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA2VoTgYifgFnRUCTyuAE6FZwUUmpqIc8WpMsuLNIrPoSOM/JJRl6osb27kMJ/8mlQP0gAWUMepPccsq56bYluasx2sWmTML
				wKlJEMtGorZIpayjDCvro4BomFKU6214dozu9+Douu8B6D82HFtrGmzU0IsqejdXr53z/DwQXSw0JOLq1MjZagwkcorWd+1wyUXG8YR4rUc4f9XFxdB+D7E5sZ8FafhM8ijnIOQbbK
				oTc5IzfQUPtCFoqAWe46hv+7cQK6tFhbkXd8SXlu1MBugT2IyZqX3izsSsavzxBM5+zeVinYSyuj5TylUWIobwfA5awhWe9llWPyxvtjYQwHHwIDAQAB
			*/
			Console.WriteLine ("PROVA 1");
			string value = Security.Unify (
				new string[] { "wKlJEMtGorZIpayjDCvro4BomFKU6214dozu9+Douu8B6D82HFtrGmzU0IsqejdXr53z/DwQXSw0JOLq1MjZagwkcorWd+1wyUXG8YR4rUc4f9XFxdB+D7E5sZ8FafhM8ijnIOQbbK", 
					"oTc5IzfQUPtCFoqAWe46hv+7cQK6tFhbkXd8SXlu1MBugT2IyZqX3izsSsavzxBM5+zeVinYSyuj5TylUWIobwfA5awhWe9llWPyxvtjYQwHHwIDAQAB", 
					"MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA2VoTgYifgFnRUCTyuAE6FZwUUmpqIc8WpMsuLNIrPoSOM/JJRl6osb27kMJ/8mlQP0gAWUMepPccsq56bYluasx2sWmTML" }, 
				new int[] { 2,0,1 });
			Console.WriteLine ("PROVA 2");
			// Create a new connection to the Google Play Service
			_serviceConnection = new InAppBillingServiceConnection (this, value);
			_serviceConnection.OnConnected += () => {
				// Attach to the various error handlers to report issues
				_serviceConnection.BillingHandler.OnGetProductsError += (int responseCode, Bundle ownedItems) => {
					Console.WriteLine("Error getting products");
				};

				_serviceConnection.BillingHandler.OnInvalidOwnedItemsBundleReturned += (Bundle ownedItems) => {
					Console.WriteLine("Invalid owned items bundle returned");
				};

				_serviceConnection.BillingHandler.OnProductPurchasedError += (int responseCode, string sku) => {
					Console.WriteLine("Error purchasing item {0}",sku);
				};

				_serviceConnection.BillingHandler.OnPurchaseConsumedError += (int responseCode, string token) => {
					Console.WriteLine("Error consuming previous purchase");
				};

				_serviceConnection.BillingHandler.InAppBillingProcesingError += (message) => {
					Console.WriteLine("In app billing processing error {0}",message);
				};

				_serviceConnection.BillingHandler.QueryInventoryError += (int responseCode, Bundle skuDetails) => {
					Console.WriteLine("Query Inventory error:"+responseCode);
				};

				_serviceConnection.BillingHandler.BuyProductError +=(int responseCode, string sku) =>{
					Console.WriteLine("Buy Product Error:"+responseCode+"|"+sku);
				};

				_serviceConnection.BillingHandler.OnProductPurchased +=(int response, Purchase purchase, string purchaseData, string purchaseSignature) => {
					Console.WriteLine("On Product Purchased:"+response+"|"+purchase.OrderId+"|"+purchaseData+"|"+purchaseSignature);

					int exit = 0;
					while(exit == 0){

						exit = controlloAcquisto(purchaseData,purchaseSignature); 

					}

					if(exit == 1){
						GetPurchase();
					}
					if(exit == 2){
						AlertDialog.Builder alert = new AlertDialog.Builder (this);
						alert.SetTitle("Errore");
						alert.SetMessage ("Pagamento non valido riprovare con credenziali corrette");
						alert.SetPositiveButton ("Ok", (senderAlert, args) => {
							GetPurchase();
						});
						alert.Show();
					}
				};

				Console.WriteLine ("PROVA 5");
				// Load inventory or available products
				GetInventory();
				Console.WriteLine ("PROVA 9");
				// Load any items already purchased
				//LoadPurchasedItems();
			};
			Console.WriteLine ("PROVA 3");
			// Attempt to connect to the service
			_serviceConnection.Connect ();
			Console.WriteLine ("PROVA 4");
		}

		private async Task GetInventory ()
		{
			Console.WriteLine ("PROVA 6");
			// Ask the open connection's billing handler to return a list of avilable products for the 
			// given list of items.
			// NOTE: We are asking for the Reserved Test Product IDs that allow you to test In-App
			// Billing without actually making a purchase.
			IList<Product> _products = await _serviceConnection.BillingHandler.QueryInventoryAsync (new List<string> {
				"lovie.10messaggi",
				"lovie.50messaggi",
				"lovie.200messaggi",
				"lovie.500messaggi",
				"lovie.1000messaggi"
			}, ItemType.Product);


			Console.WriteLine ("PROVA 7"+_products.Count);
			// Were any products returned?
			if (_products == null) {
				// No, abort
				return;
			}

			if (_products.Count != 0) {
		
				Console.WriteLine ("PROVA 7.1");
				RunOnUiThread (() => {
					Prodotti= new List<Product>();

					Prodotti.Add(new Product());
					Prodotti.Add(new Product());
					Prodotti.Add(new Product());
					Prodotti.Add(new Product());
					Prodotti.Add(new Product());
				});
				//Prodotti = _products;
				Console.WriteLine ("PROVA 7.2");
				for (int i = 0; i < _products.Count; i++) {
					Console.WriteLine ("PROVA 7/"+i+":"+_products [i].ProductId);
					if (_products [i].ProductId == "lovie.10messaggi")
						RunOnUiThread(() =>{
							Console.WriteLine ("PROVA 7/3/"+i);
							Prodotti [0] = _products [i];
						});

					if (_products [i].ProductId == "lovie.50messaggi")
						RunOnUiThread(() =>{
							Console.WriteLine ("PROVA 7/3/"+i);
							Prodotti [1] = _products [i];
						});

					if (_products [i].ProductId == "lovie.200messaggi")
						RunOnUiThread(() =>{
							Console.WriteLine ("PROVA 7/3/"+i);
							Prodotti [2] = _products [i];
						});

					if (_products [i].ProductId == "lovie.500messaggi")
						RunOnUiThread(() =>{
							Console.WriteLine ("PROVA 7/3/"+i);
							Prodotti [3] = _products [i];
						});

					if (_products [i].ProductId == "lovie.1000messaggi")
						RunOnUiThread(() =>{
							Console.WriteLine ("PROVA 7/3/"+i+"|"+Prodotti.Count);
							Prodotti [4] = _products [i];
						});

				}
				Console.WriteLine ("PROVA 7.3");
				/*
				Product1.Enabled = true;
				Product2.Enabled = true;
				Product3.Enabled = true;
				Product4.Enabled = true;
				Product5.Enabled = true;

				Product1.SetBackgroundColor (Color.Rgb(63,148,202));
				Product2.SetBackgroundColor (Color.Rgb(87,196,241));
				Product3.SetBackgroundColor (Color.Rgb(63,148,202));
				Product4.SetBackgroundColor (Color.Rgb(87,196,241));
				Product5.SetBackgroundColor (Color.Rgb(63,148,202));

				*/

				Console.WriteLine ("PROVA 8:" + Prodotti.Count);
				for (int i = 0; i < Prodotti.Count; i++) {


					Console.WriteLine ("N°" + i + ":" + Prodotti [i].Description + "|" + Prodotti [i].Title + "|" + Prodotti [i].ProductId + "|" + Prodotti [i].Price);

					if (i == 0) {
						Product1.Enabled = true;
						Price1.Text = Prodotti [i].Price;
						Product1.SetBackgroundColor (Color.Rgb(63,148,202));
					}
					if (i == 1) {
						Product2.Enabled = true;
						Price2.Text = Prodotti [i].Price;
						Product2.SetBackgroundColor (Color.Rgb(87,196,241));
					}
					if (i == 2) {
						Product3.Enabled = true;
						Price3.Text = Prodotti [i].Price;
						Product3.SetBackgroundColor (Color.Rgb(63,148,202));
					}
					if (i == 3) {
						Product4.Enabled = true;
						Price4.Text = Prodotti [i].Price;
						Product4.SetBackgroundColor (Color.Rgb(87,196,241));
					}
					if (i == 4) {
						Product5.Enabled = true;
						Price5.Text = Prodotti [i].Price;
						Product5.SetBackgroundColor (Color.Rgb(63,148,202));
					}
					if(i==1)
						Price2.Text = Prodotti[i].Price;
					if(i==2)
						Price3.Text = Prodotti[i].Price;
					if(i==3)
						Price4.Text = Prodotti[i].Price;
					if(i==4)
						Price5.Text = Prodotti[i].Price;

				}
			} else
				return;

			//GetPurchase ();
			// Enable the list of products
			/*_produtctSpinner.Enabled = (_products.Count > 0);

			// Populate list of available products
			var items = _products.Select (p => p.Title).ToList ();
			_produtctSpinner.Adapter = 
				new ArrayAdapter<string> (this, 
					Android.Resource.Layout.SimpleSpinnerItem,
					items);
					*/
		}

		protected override void OnDestroy ()
		{
			// Are we attached to the Google Play Service?
			if (_serviceConnection != null) {
				// Yes, disconnect
				_serviceConnection.Disconnect ();
			}

			// Call base method
			base.OnDestroy ();
		}


		public void PurchaseProduct(int i){
			RunOnUiThread (() => {
				Console.WriteLine ("1a");
				var product = Prodotti [i];
				_serviceConnection.BillingHandler.BuyProduct (product);
				Console.WriteLine ("1b");
			});
		}

		protected override void OnActivityResult (int requestCode, Result resultCode, Intent data)
		{
			// Ask the open service connection's billing handler to process this request
			Console.WriteLine ("2a");
			_serviceConnection.BillingHandler.HandleActivityResult (requestCode, resultCode, data);
			Console.WriteLine ("2b");
			//TODO: Use a call back to update the purchased items
			//GetPurchase ();
		}

		private void GetPurchase ()
		{
			// Ask the open connection's billing handler to get any purchases
			IList<Purchase> purchases = _serviceConnection.BillingHandler.GetPurchases (ItemType.Product);
			Console.WriteLine ("3:"+purchases.Count);
			if (purchases.Count == 0)
				return;
			Purchase prod=purchases [purchases.Count - 1];
			ConsumeProduct (prod);
		}

		public void ConsumeProduct(Purchase prod){

			bool result = _serviceConnection.BillingHandler.ConsumePurchase (prod);
			Console.WriteLine (4+" "+result);
				// Was the product consumed?
			if (result) {
					// Yes, update interface
				Console.WriteLine ("COMPRATO");
				Toast.MakeText (ApplicationContext, "Acquisto completato con successo", ToastLength.Long).Show();

			}
		}

		public int controlloAcquisto(string data,string signature){

			//DEVELOPMENT
			//var client = new RestClient("http://location.keepup.pro:3000/");
			//DISTRIBUTION
			var client = new RestClient("http://api.netwintec.com:3000/");


			var requestN4U = new RestRequest("api/user/update_credit", Method.POST);
			requestN4U.AddHeader("content-type", "application/json");
			requestN4U.AddHeader ("lovie-token", prefs.GetString ("TokenN4U", ""));


			JObject oJsonObject = new JObject();

			oJsonObject.Add("data", data);
			oJsonObject.Add("signature", signature);
			oJsonObject.Add("type", "android");

			requestN4U.AddParameter("application/json; charset=utf-8", oJsonObject, ParameterType.RequestBody);


			var response = client.Execute (requestN4U);

			if (response.StatusCode == System.Net.HttpStatusCode.OK) {

				return 1;

			}
			if (response.StatusCode == System.Net.HttpStatusCode.NotFound)
			{

				return 2;

			}
			return 0;

		}
	}
}

