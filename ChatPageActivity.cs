﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Json;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Content.PM;
using Android.Graphics;
using Android.Views.InputMethods;
using Android.Views.Animations;
using Android.Animation;

using RestSharp;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Badge.Plugin;

namespace Lovie
{
	[Activity (Label = "ChatPageActivity",Theme = "@style/AppTheme",ScreenOrientation = ScreenOrientation.Portrait,NoHistory =true)]			
	public class ChatPageActivity : BaseActivity
	{

		public const string ERR_MAX_PRELIMINAR_MESSAGE = "Max preliminary message for this person reached";
		public const string ERR_NOT_ENOUGHT_CREDIT = "Credit is not enough";

		ImageView menuIcon;
		ImageView backIcon;
		ImageView segnIcon;

		MyEditText message;
		ImageView sendButton;

		ListView ListChat;
		ListChatAdapter adapter;
		public List<Message> list = new List<Message>();

		InputMethodManager imm;

		private int iHeightDefault;
		private RelativeLayout rootView;


		public ProgressBar progress;
		public bool KeyboardIsOpen = false;
		public bool removeNot = false;

		int Indice = 0;

		string key,nome,FacebookId,number;
		bool isPreliminary,isFirstMessage;
		bool change = false;
		bool isRevealed = true;

		public string SegnalaKey, SegnalaPersonaName, SegnalaPersonaId;

		public static ChatPageActivity Instance {get;private set;}

		protected override void OnCreate (Bundle savedInstanceState)
		{
			base.OnCreate (savedInstanceState);

			ChatPageActivity.Instance = this;

			if(Intent.HasExtra("key"))
				key = Intent.GetStringExtra ("key");
			if(Intent.HasExtra("name"))
				nome = Intent.GetStringExtra ("name");
			if(Intent.HasExtra("FacebookId"))
				FacebookId = Intent.GetStringExtra ("FacebookId");
			if(Intent.HasExtra("Number"))
				number = Intent.GetStringExtra ("Number");


			isPreliminary = Intent.GetBooleanExtra ("isPreliminary",false);
			isFirstMessage = Intent.GetBooleanExtra ("isFirstMessage",false);

			ChatPageActivity.Instance = this;
			//Window.SetSoftInputMode (SoftInput.AdjustResize);//.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
			Window.SetSoftInputMode (SoftInput.StateHidden);


			MainActivity.Instance.isHome = false;
			MainActivity.Instance.isChat = true;
			MainActivity.Instance.ChatKey = key;
			MainActivity.Instance.isNothing = false;
			Console.WriteLine ("c1:" + MainActivity.Instance.isStopped);
			MainActivity.Instance.isStopped = false;
			Console.WriteLine ("c2:" + MainActivity.Instance.isStopped);

			imm = (InputMethodManager)GetSystemService(Context.InputMethodService);
			//imm.ToggleSoftInput(ShowFlags.Implicit,HideSoftInputFlags.None);
			Console.WriteLine(imm.IsAcceptingText);
			Typeface RobotoR = Typeface.CreateFromAsset(Assets,"fonts/Roboto_Regular.ttf");

			menuIcon = (ImageView) getToolbar().FindViewById(Resource.Id.menu_icon);
			backIcon = (ImageView) getToolbar().FindViewById(Resource.Id.back_icon);
			segnIcon = (ImageView)getToolbar().FindViewById(Resource.Id.segn_icon);

			setActionBarIcon(Resource.Drawable.reveal,Resource.Drawable.back,Resource.Drawable.segnala);

			menuIcon.Visibility = ViewStates.Invisible;
			backIcon.Visibility = ViewStates.Visible;
			segnIcon.Visibility = ViewStates.Visible;
			//mDrawerLayout.SetDrawerShadow(R.drawable.drawer_shadow,GravityCompat.START);

			segnIcon.Click += delegate
			{
				if (isPreliminary)
				{

					AlertDialog.Builder alert = new AlertDialog.Builder(this);
					alert.SetTitle("Errore");
					alert.SetMessage("Impossibile segnalare un utente che non è registrato all'app");
					alert.SetPositiveButton("Continua", (senderAlert, args) => { });
					alert.SetCancelable(false);
					alert.Show();

				}
				else {
					var inputDialog = new AlertDialog.Builder(this);
					EditText userInput = new EditText(this);
					userInput.Hint = "Motivo segnalazione";
					//SetEditTextStylings(userInput);
					userInput.InputType = Android.Text.InputTypes.TextFlagMultiLine;
					inputDialog.SetTitle("Segnala");
					inputDialog.SetCancelable(false);
					inputDialog.SetMessage("Inviaci una segnalazione per questa conversazione specificando , se vuoi, il motivo della segnalazione.");
					inputDialog.SetView(userInput);
					inputDialog.SetPositiveButton("Ok", (see, ess) =>
					{

						//var client = new RestClient("http://location.keepup.pro:3000/");
						//DISTRIBUTION
						var client = new RestClient("http://api.netwintec.com:3000/");

						ISharedPreferences prefs = MainActivity.Instance.prefs;
						string token = prefs.GetString("TokenN4U", "");

						if (token == "")
						{

							// ERRORE
							return;

						}

						var requestN4U = new RestRequest("api/conversations/report", Method.POST);
						requestN4U.AddHeader("content-type", "application/json");
						requestN4U.AddHeader("lovie-token", token);
						requestN4U.AddHeader("lovie-conversation-token", key);

						JObject oJsonObject = new JObject();

						oJsonObject.Add("description", userInput.Text);

						requestN4U.AddParameter("application/json; charset=utf-8", oJsonObject, ParameterType.RequestBody);


						client.ExecuteAsync(requestN4U, (s, e) =>
						{

							RunOnUiThread(() =>
							{

								IRestResponse response = s;
								Console.WriteLine("RESPONSE:" + response.Content + "|" + response.StatusCode);
								if (response.StatusCode == System.Net.HttpStatusCode.OK)
								{
									RunOnUiThread(() =>
									{
										Toast.MakeText(ApplicationContext, "Segnalazione avvenuta con successo", ToastLength.Long).Show();
									});
								}
								else
								{
									RunOnUiThread(() =>
									{
										Toast.MakeText(ApplicationContext, "Segnalazione avvenuta con successo", ToastLength.Long).Show();
									});
								}
							});
						});

						Console.WriteLine(userInput.Text + "|" + SegnalaKey + "|" + SegnalaPersonaId + "|" + SegnalaPersonaName);
						HideKeyboard(userInput);
					});

					inputDialog.SetNegativeButton("Annulla", (afk, kfa) =>
					{
						Console.WriteLine("ANNULLA");
						HideKeyboard(userInput);
					});

					inputDialog.Show();
					ShowKeyboard(userInput);
				}
			};

			backIcon.Click += delegate {
				
				if(isFirstMessage){

					if(Indice == 0){
						var intent = new Intent (this, typeof(AddChatPageActivity));
						this.StartActivity (intent);
						//Finish();
					}
					else{
						var intent = new Intent (this, typeof(HomePageActivity));
						this.StartActivity (intent);
						change = true;
						//Finish();
					}

				}
				else{
					var intent = new Intent (this, typeof(HomePageActivity));
					this.StartActivity (intent);
					change = true;
					//Finish();
				}
			};


			menuIcon.Click += delegate {
				if(!isRevealed){

					AlertDialog.Builder alert = new AlertDialog.Builder (this);
					alert.SetTitle("Rivelati");
					alert.SetMessage ("Vuoi veramente rivelarti?");
					alert.SetPositiveButton ("Ok", (senderAlert, args) => {
						//var client = new RestClient("http://location.keepup.pro:3000/");
						//DISTRIBUTION
						var client = new RestClient("http://api.netwintec.com:3000/");

						ISharedPreferences prefs = MainActivity.Instance.prefs;
						string token = prefs.GetString ("TokenN4U", "");

						if (token == "")
						{

							// ERRORE
							return;

						}

						var requestN4U = new RestRequest("api/conversations/reveal", Method.POST);
						requestN4U.AddHeader("content-type", "application/json");
						requestN4U.AddHeader("lovie-token", token);
						requestN4U.AddHeader("lovie-conversation-token", key);

						IRestResponse response = client.Execute(requestN4U);

						if (response.StatusCode == System.Net.HttpStatusCode.OK)
						{
							menuIcon.Visibility = ViewStates.Invisible;
							isRevealed = true;
							Toast.MakeText (ApplicationContext, "Ti sei Rivelato con successo", ToastLength.Long).Show();

						}
						else {
							isRevealed = false;
							Toast.MakeText (ApplicationContext, "Errore nella rilevazione provare più tardi", ToastLength.Long).Show();
						}
					});
					alert.SetNegativeButton("Annulla",(senderAlert, args) => {} );
					alert.Show();
				}
			};
			//message = FindViewById<EditText> (Resource.Id.Message);

			message = new MyEditText (ApplicationContext);
			RelativeLayout.LayoutParams rl = new RelativeLayout.LayoutParams(0,0);
			rl = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MatchParent,RelativeLayout.LayoutParams.WrapContent);


			rl.SetMargins(PixelsToDp(15),PixelsToDp(10),PixelsToDp(50),PixelsToDp(10));
			rl.AddRule (LayoutRules.CenterVertical);

			message.LayoutParameters  = rl;
			message.Hint = "Scrivi un messaggio";
			message.InputType = Android.Text.InputTypes.ClassText;
			message.SetSingleLine (false);
			message.SetBackgroundColor (Color.White);
			//message.ImeOptions = ImeAction.Unspecified;
			message.SetHintTextColor (Color.Argb (255, 80, 80, 80));
			message.InputType =Android.Text.InputTypes.ClassText | Android.Text.InputTypes.TextFlagMultiLine;
			message.SetMaxLines(4);
			message.SetTextColor (Color.Black);
			message.SetTextSize (Android.Util.ComplexUnitType.Dip,15);
			message.Typeface = RobotoR;

			FindViewById<RelativeLayout> (Resource.Id.relativeLayout2).AddView (message);


			sendButton = FindViewById<ImageView> (Resource.Id.SendButton);

			Console.WriteLine ("1");

			Console.WriteLine ("2");

			progress = FindViewById<ProgressBar>(Resource.Id.progressBar1);

			/*var a = ObjectAnimator.OfFloat (svgView,"percentage",0.0f,1.0f);
			a.SetDuration (1000);
			a.RepeatCount = int.MaxValue;
			a.SetInterpolator (new LinearInterpolator());
			a.Start();*/

			FindViewById<LinearLayout> (Resource.Id.sopra).Visibility = ViewStates.Visible;

			if(!isFirstMessage)
				RetriveMessages ();
			else
				FindViewById<LinearLayout> (Resource.Id.sopra).Visibility = ViewStates.Gone;
			/*
			list.Add(new Message("Prova",false));
			list.Add(new Message("Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua", false));
			list.Add(new Message("Prova", true));
			list.Add(new Message("Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua", false));
			list.Add(new Message("😎😎😎😎😎😎😎😎", true));
			list.Add(new Message("Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua", true));
			list.Add(new Message("Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua", false));
			list.Add(new Message("Prova", false));
			list.Add(new Message("😆❤️👍🏿👍🏼😏☺️😇👦🏿😁😑", true));
			list.Add(new Message("Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua", true));
			*/

			ListChat = FindViewById<ListView> (Resource.Id.ListChat);
			ListChat.SetFooterDividersEnabled (false);
			ListChat.SetHeaderDividersEnabled (false);
			ListChat.Divider.SetAlpha (0);
			ListChat.ItemClick += delegate {
				Console.WriteLine("aa "+KeyboardIsOpen);

				if(KeyboardIsOpen){
					imm.HideSoftInputFromWindow(CurrentFocus.WindowToken,HideSoftInputFlags.None);
					closeKeyboard ();
					KeyboardIsOpen = false;
				}
				Console.WriteLine("aa2 "+KeyboardIsOpen);
			};


			adapter = new ListChatAdapter (this,list);

			ListChat.Adapter = adapter;

			sendButton.Click+= delegate {

				if(!string.IsNullOrEmpty(message.Text) && !string.IsNullOrWhiteSpace(message.Text)){
					if(!isPreliminary){
						Console.WriteLine("MESSAGE:"+message.Text);
						// ******** SEND MESSAGE **************
						//var client = new RestClient("http://location.keepup.pro:3000/");
						//DISTRIBUTION
						var client = new RestClient("http://api.netwintec.com:3000/");

						ISharedPreferences prefs = MainActivity.Instance.prefs;
						string token = prefs.GetString ("TokenN4U", "");

						if (token != "") {

							var requestN4U = new RestRequest("api/conversations/message", Method.POST);
							requestN4U.AddHeader("content-type", "application/json");
							requestN4U.AddHeader("lovie-token",token);
							requestN4U.AddHeader("lovie-conversation-token", key);

							JObject oJsonObject = new JObject();

							oJsonObject.Add("message", message.Text);

							requestN4U.AddParameter("application/json; charset=utf-8", oJsonObject, ParameterType.RequestBody);

							IRestResponse response = client.Execute(requestN4U);

							Console.WriteLine(response.Content+"|"+response.StatusCode);

							if(response.StatusCode == System.Net.HttpStatusCode.OK){
								adapter.listChat.Add(new Message(message.Text,true));
								adapter.NotifyDataSetChanged();
								ScrollToEnd ();
								message.Text="";
								Indice++;
							}
							if(response.StatusCode == System.Net.HttpStatusCode.NotFound){

								if(response.Content.CompareTo(ERR_MAX_PRELIMINAR_MESSAGE)==0){
									AlertDialog.Builder alert = new AlertDialog.Builder (this);
									alert.SetTitle("Messaggio Preliminare");
									alert.SetMessage ("Hai raggiunto il numero massimo (3) di messaggi inviabili ad un utente non registrato aspetta che ti risponda per poter continuare");
									alert.SetPositiveButton ("Ok", (senderAlert, args) => {} );
									alert.Show();
								}
								if(response.Content.CompareTo(ERR_NOT_ENOUGHT_CREDIT)==0){
									AlertDialog.Builder alert = new AlertDialog.Builder (this);
									alert.SetTitle("Credito Indufficente");
									alert.SetMessage ("Credito insufficente, ricarica per poter continuare a chattare ");
									alert.SetPositiveButton ("Ok", (senderAlert, args) => {} );
									alert.Show();
								}

							}

						}
					}
					else{
						//WA MESSAGE
						if(!string.IsNullOrEmpty(number)){
							Console.WriteLine("PRELIMINARY_WA_MESSAGE:"+message.Text+"|"+number);
							// ******** SEND PRELIMINARY MESSAGE **************
							//var client = new RestClient("http://location.keepup.pro:3000/");
							//DISTRIBUTION
							var client = new RestClient("http://api.netwintec.com:3000/");

							ISharedPreferences prefs = MainActivity.Instance.prefs;
							string token = prefs.GetString ("TokenN4U", "");

							if (token != "") {

								var requestN4U = new RestRequest("api/conversations/preliminary_message", Method.POST);
								requestN4U.AddHeader("content-type", "application/json");
								requestN4U.AddHeader("lovie-token",token);

								JObject oJsonObject = new JObject();

								oJsonObject.Add("message", message.Text);
								oJsonObject.Add("phone", number);

								requestN4U.AddParameter("application/json; charset=utf-8", oJsonObject, ParameterType.RequestBody);

								IRestResponse response = client.Execute(requestN4U);

								Console.WriteLine(response.Content+"|"+response.StatusCode);

								if(response.StatusCode == System.Net.HttpStatusCode.OK){
									adapter.listChat.Add(new Message(message.Text,true));
									adapter.NotifyDataSetChanged();
									ScrollToEnd ();
									message.Text="";
									Indice++;
								}
								if(response.StatusCode == System.Net.HttpStatusCode.NotFound){

									if(response.Content.CompareTo(ERR_MAX_PRELIMINAR_MESSAGE)==0){
										AlertDialog.Builder alert = new AlertDialog.Builder (this);
										alert.SetTitle("Messaggio Preliminare");
										alert.SetMessage ("Hai raggiunto il numero massimo (3) di messaggi inviabili ad un utente non registrato aspetta che ti risponda per poter continuare");
										alert.SetPositiveButton ("Ok", (senderAlert, args) => {} );
										alert.Show();
									}
									if(response.Content.CompareTo(ERR_NOT_ENOUGHT_CREDIT)==0){
										AlertDialog.Builder alert = new AlertDialog.Builder (this);
										alert.SetTitle("Credito Indufficente");
										alert.SetMessage ("Credito insufficente, ricarica per poter continuare a chattare ");
										alert.SetPositiveButton ("Ok", (senderAlert, args) => {} );
										alert.Show();
									}

								}

							}
						}
						//FB MESSAGE
						if(!string.IsNullOrEmpty(FacebookId)){
							Console.WriteLine("PRELIMINARY_FB_MESSAGE:"+message.Text+"|"+FacebookId);
							// ******** SEND PRELIMINARY MESSAGE **************
							//var client = new RestClient("http://location.keepup.pro:3000/");
							//DISTRIBUTION
							var client = new RestClient("http://api.netwintec.com:3000/");

							ISharedPreferences prefs = MainActivity.Instance.prefs;
							string token = prefs.GetString ("TokenN4U", "");

							if (token != "") {

								var requestN4U = new RestRequest("api/conversations/preliminary_message", Method.POST);
								requestN4U.AddHeader("content-type", "application/json");
								requestN4U.AddHeader("lovie-token",token);

								JObject oJsonObject = new JObject();

								oJsonObject.Add("message", message.Text);
								oJsonObject.Add("socialId", FacebookId);
								oJsonObject.Add("socialFullName", nome);
								oJsonObject.Add("socialName", "facebook");

								requestN4U.AddParameter("application/json; charset=utf-8", oJsonObject, ParameterType.RequestBody);

								IRestResponse response = client.Execute(requestN4U);

								Console.WriteLine(response.Content+"|"+response.StatusCode);

								if(response.StatusCode == System.Net.HttpStatusCode.OK){
									adapter.listChat.Add(new Message(message.Text,true));
									adapter.NotifyDataSetChanged();
									ScrollToEnd ();
									message.Text="";
									Indice++;
								}
								if(response.StatusCode == System.Net.HttpStatusCode.NotFound){

									if(response.Content.CompareTo(ERR_MAX_PRELIMINAR_MESSAGE)==0){
										AlertDialog.Builder alert = new AlertDialog.Builder (this);
										alert.SetTitle("Messaggio Preliminare");
										alert.SetMessage ("Hai raggiunto il numero massimo (3) di messaggi inviabili ad un utente non registrato aspetta che ti risponda per poter continuare");
										alert.SetPositiveButton ("Ok", (senderAlert, args) => {} );
										alert.Show();
									}
									if(response.Content.CompareTo(ERR_NOT_ENOUGHT_CREDIT)==0){
										AlertDialog.Builder alert = new AlertDialog.Builder (this);
										alert.SetTitle("Credito Indufficente");
										alert.SetMessage ("Credito insufficente, ricarica per poter continuare a chattare ");
										alert.SetPositiveButton ("Ok", (senderAlert, args) => {} );
										alert.Show();
									}

								}
							}
						}
					}
				}

			};

			message.Click += delegate {
				Console.WriteLine(imm.IsAcceptingText);

				//if(!KeyboardIsOpen)
				//	ScrollToEnd();



			};
				

			FindViewById<TextView> (Resource.Id.Nome).Text = nome;

			//closeKeyboard ();
			ScrollToEnd ();

			rootView = FindViewById<RelativeLayout> (Resource.Id.root);

			Rect r = new Rect();
			View view = Window.DecorView;
			view.GetWindowVisibleDisplayFrame(r);
			iHeightDefault = r.Bottom;

			rootView.ViewTreeObserver.GlobalLayout += (sender, e) => {
				Rect re = new Rect();
				View viewR = Window.DecorView;
				viewR.GetWindowVisibleDisplayFrame(re);

				if(iHeightDefault > re.Bottom){
					//Keyboard is Show
					Console.WriteLine("SHOW2 "+KeyboardIsOpen);
					if(!KeyboardIsOpen){
						ScrollToEnd();
						KeyboardIsOpen= true;
					}
					Console.WriteLine("SHOW "+KeyboardIsOpen);
				} else {
					Console.WriteLine("HIDE");
					if(KeyboardIsOpen)
						KeyboardIsOpen= false;
					//Keyboard is Hidden
				}
			};

			// Create your application here


		}

		private int PixelsToDp(int pixels)
		{
			return (int)Android.Util.TypedValue.ApplyDimension(Android.Util.ComplexUnitType.Dip, pixels, Resources.DisplayMetrics);
		}

		public void closeKeyboard(){

			Window.SetSoftInputMode (SoftInput.StateHidden);

		}

		protected override void OnStop ()
		{
			Console.WriteLine("s1:"+MainActivity.Instance.isStopped);
			if (!change) {
				MainActivity.Instance.isStopped = true;
			} else {
				change = false;
			}
			Console.WriteLine("s2:"+MainActivity.Instance.isStopped);
			base.OnStop ();
		}

		protected override void OnRestart ()
		{
			Console.WriteLine("r1:"+MainActivity.Instance.isStopped);
			MainActivity.Instance.isStopped = false;
			Console.WriteLine("r2:"+MainActivity.Instance.isStopped);

			if (removeNot) {

				removeNot = false;
				NotificationManager notifManager= (NotificationManager) ApplicationContext.GetSystemService(Context.NotificationService);
				notifManager.CancelAll();

				FindViewById<LinearLayout>(Resource.Id.sopra).Visibility = ViewStates.Visible;

				list.Clear();
				adapter.NotifyDataSetChanged();
				ScrollToEnd();

				RetriveMessages();
			}

			base.OnRestart ();
		}
			

		public override void OnBackPressed ()
		{
			if(isFirstMessage){

				if(Indice == 0){
					var intent = new Intent (this, typeof(AddChatPageActivity));
					this.StartActivity (intent);
					//Finish();
				}
				else{
					var intent = new Intent (this, typeof(HomePageActivity));
					this.StartActivity (intent);
					change = true;
					//Finish();
				}

			}
			else{
				var intent = new Intent (this, typeof(HomePageActivity));
				this.StartActivity (intent);
				change = true;
				//Finish();
			}
		}


		protected override int getLayoutResource() {
			return Resource.Layout.ChatPage;
		}

		private void ScrollToEnd(){
			ListChat.Post(new Scroll(ListChat));
		}


		// ****************** RETRIVE CONVERSATION'S MESSAGE FROM SERVER ***********************

		public void RetriveMessages(){


			//var client = new RestClient("http://location.keepup.pro:3000/");
			//DISTRIBUTION
			var client = new RestClient("http://api.netwintec.com:3000/");

			ISharedPreferences prefs = MainActivity.Instance.prefs;
			string token = prefs.GetString ("TokenN4U", "");

			if (token == "") {

				// ERRORE
				return;

			}

			var requestN4U = new RestRequest("api/conversations/get_conversation/"+key+"/"+token, Method.GET);
			requestN4U.AddHeader("content-type", "application/json");


			//IRestResponse response = 
			client.ExecuteAsync(requestN4U,(s,e) => {

				RunOnUiThread(() => {
					IRestResponse response = s;

					Console.WriteLine ("RESPONSE:" + response.Content + "|" + response.StatusCode);
					if (response.StatusCode == System.Net.HttpStatusCode.OK) {

						List <Message> listAppoggio = new List<Message> ();

						JsonValue json = JsonValue.Parse (response.Content);

						int reveal =0;
						bool mine = false;

						foreach (JsonValue j in json) {

							var messages = j ["messages"];
							reveal = j["isRevealed"];
							mine = j["isStartedByMe"];

							SegnalaKey = j["_key"];

							string[] IDApp = SegnalaKey.Split(':');
							if (mine)
							{
								SegnalaPersonaId = IDApp[2] + "::" + IDApp[4] + "::" + IDApp[6];
							}
							else
							{
								SegnalaPersonaId = IDApp[8] + "::" + IDApp[10] + "::" + IDApp[12];
							}
							try
							{
								int NotReadMessage = j["unreadMessages"];

								var prefEditor = prefs.Edit();
								prefEditor.PutInt("Badge", NotReadMessage);
								prefEditor.Commit();

								CrossBadge.Current.SetBadge(NotReadMessage);
							}
							catch (Exception ee){}

							foreach (JsonValue jj in messages) {

								string message = jj ["message"];
								bool isMine = jj ["isFromMe"];

								if (isMine)
									SegnalaPersonaName = jj["to"];
								else
									SegnalaPersonaName = jj["from"];
								
								Message m = new Message (message,isMine);
								listAppoggio.Add (m);

							}
						}
						

							for (int i = listAppoggio.Count - 1; i >= 0; i--) {

								list.Add(listAppoggio [i]);
								//list.Add (listAppoggio [i]);
								//Enumerable.Reverse (list);
							}
							if(mine){
								if (reveal == 0)
								{
									menuIcon.Visibility = ViewStates.Visible;
									isRevealed = false;
								}
							}

							//adapter.listChat.Add(new Message(messageT,false));
							adapter.NotifyDataSetChanged();
							ScrollToEnd ();

							FindViewById<LinearLayout> (Resource.Id.sopra).Visibility = ViewStates.Gone;

						
					}
					else{
						//var intent = new Intent (this, typeof(HomePageActivity));
						//StartActivity(intent);
					}
				});
			});
				
		}

		// ****************** ADD MESSAGE AFTER NOTIFICATION ***********************

		public void AddNewMessage(string messageT){

			//var client = new RestClient("http://location.keepup.pro:3000/");
			//DISTRIBUTION
			var client = new RestClient("http://api.netwintec.com:3000/");

			ISharedPreferences prefs = MainActivity.Instance.prefs;
			string token = prefs.GetString ("TokenN4U", "");

			if (token == "") {
				return;
			}

			var requestN4U = new RestRequest("api/conversations/get_conversation/"+key+"/"+token, Method.GET);
			requestN4U.AddHeader("content-type", "application/json");

			client.ExecuteAsync(requestN4U, (s,e) => {

				RunOnUiThread(() =>
				{

					IRestResponse response = s;
					Console.WriteLine("RESPONSE:" + response.Content + "|" + response.StatusCode);
					if (response.StatusCode == System.Net.HttpStatusCode.OK)
					{

						List<Message> listAppoggio = new List<Message>();

						JsonValue json = JsonValue.Parse(response.Content);
						foreach (JsonValue j in json)
						{

							int NotReadMessage = j["unreadMessages"];

							var prefEditor = prefs.Edit();
							prefEditor.PutInt("Badge", NotReadMessage);
							prefEditor.Commit();

							CrossBadge.Current.SetBadge(NotReadMessage);

						}
					}
				});

			});
				
			RunOnUiThread(() => {
				list.Add(new Message(messageT,false));
				adapter.NotifyDataSetChanged();
				ScrollToEnd ();

			});

		}

		public void ShowKeyboard(EditText userInput)
		{
			userInput.RequestFocus();
			InputMethodManager imm = (InputMethodManager)this.GetSystemService(Context.InputMethodService);
			imm.ToggleSoftInput(ShowFlags.Forced, 0);
		}

		public void HideKeyboard(EditText userInput)
		{
			InputMethodManager imm = (InputMethodManager)this.GetSystemService(Context.InputMethodService);
			imm.HideSoftInputFromWindow(userInput.WindowToken, 0);
		}

	}

	public class Scroll : Java.Lang.Object,Java.Lang.IRunnable
	{

		public ListView a;

		public Scroll(ListView aa ){
			a = aa;

		} 


		public void Run(){
			a.SetSelection (a.Adapter.Count-1);
		} 
			
	}

	public class MyEditText : EditText
	{



		public MyEditText(Context c) : base (c)
		{

			//EditText (c, a);

		}

		public MyEditText(Context c,Android.Util.IAttributeSet a) : base (c,a)
		{

			//EditText (c, a);
			
		}

		public override bool OnKeyPreIme (Keycode keyCode, KeyEvent e)
		{
			Console.WriteLine ("PREIME");

			ChatPageActivity.Instance.KeyboardIsOpen = false;

			return base.OnKeyPreIme (keyCode, e);
		}

	}
}

