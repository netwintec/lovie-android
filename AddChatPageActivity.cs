﻿using System;
using System.Linq;
using System.Text;
using System.Collections.Generic;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Content.PM;
using Android.Graphics;
using Android.Provider;
using Android.Webkit;

using RestSharp;
using System.Json;

namespace Lovie
{
	[Activity (Label = "AddChatActivity",Theme = "@style/AppTheme",ScreenOrientation = ScreenOrientation.Portrait)]				
	public class AddChatPageActivity : BaseActivity
	{

		ImageView menuIcon;
		ImageView backIcon;

		LinearLayout ButtonFB,ButtonWA;
		View CorniceFB,CorniceWA,BackFB,BackWA,RightSeparator,LeftSeparator;

		RelativeLayout WAView,FBView;

		ImageView WAButtonSend;
		EditText NameFB,NumberWA;
		Button SearchContact;

		List<FbPerson> ListPeople = new List<FbPerson>();
		LinearLayout CorniceListFB;
		TextView noResult;

		bool isFacebook = true;
		bool TabellaOpen=false;

		ListFbPersonAdapter adapter;

		protected override void OnCreate (Bundle savedInstanceState)
		{
			Console.WriteLine ("11111111111111111111111111111111");
			base.OnCreate (savedInstanceState);

			//Console.WriteLine ("11111111111111111111111111111111");
			Typeface RobotoR = Typeface.CreateFromAsset(Assets,"fonts/Roboto_Regular.ttf");

			menuIcon = (ImageView) getToolbar().FindViewById(Resource.Id.menu_icon);
			backIcon = (ImageView) getToolbar().FindViewById(Resource.Id.back_icon);

			setActionBarIcon(Resource.Drawable.logo_lovie,Resource.Drawable.back);

			menuIcon.Visibility = ViewStates.Invisible;
			backIcon.Visibility = ViewStates.Visible;

			MainActivity.Instance.isHome = false;
			MainActivity.Instance.isChat = false;
			MainActivity.Instance.ChatKey = "";
			MainActivity.Instance.isNothing = true;

			//mDrawerLayout.SetDrawerShadow(R.drawable.drawer_shadow,GravityCompat.START);
			Console.WriteLine ("2");
			backIcon.Click += delegate {

				var intent = new Intent (this, typeof(HomePageActivity));
				this.StartActivity (intent);
				Finish();

			};

			ButtonFB = FindViewById<LinearLayout> (Resource.Id.FBButton);
			ButtonWA = FindViewById<LinearLayout> (Resource.Id.WAButton);

			CorniceFB = FindViewById<View> (Resource.Id.CorniceFB);
			CorniceWA = FindViewById<View> (Resource.Id.CorniceWA);
			CorniceWA.Visibility = ViewStates.Invisible;
			BackFB = FindViewById<View> (Resource.Id.FBButtonBack);
			BackWA = FindViewById<View> (Resource.Id.WAButtonBack);
			RightSeparator = FindViewById<View> (Resource.Id.RightSeparator);
			LeftSeparator = FindViewById<View> (Resource.Id.LeftSeparator);

			Console.WriteLine ("3");

			ButtonFB.Click += delegate {

				if(!isFacebook){

					WAView.Visibility = ViewStates.Invisible;
					FBView.Visibility = ViewStates.Visible;

					CorniceFB.Visibility = ViewStates.Visible;
					CorniceWA.Visibility = ViewStates.Invisible;
					BackFB.SetBackgroundColor(Color.White);
					BackWA.SetBackgroundColor(Color.Argb(255,220,220,220));
					RightSeparator.SetBackgroundColor(Color.Argb(255,76,156,206));
					LeftSeparator.SetBackgroundColor(Color.White);
					ButtonFB.SetBackgroundResource(Resource.Drawable.RoundedViewWhite);
					ButtonWA.SetBackgroundResource(Resource.Drawable.RoundedViewLightGray);
					isFacebook = true;

				}

			};

			ButtonWA.Click += delegate {

				if(isFacebook){

					WAView.Visibility = ViewStates.Visible;
					FBView.Visibility = ViewStates.Invisible;

					CorniceFB.Visibility = ViewStates.Invisible;
					CorniceWA.Visibility = ViewStates.Visible;
					BackFB.SetBackgroundColor(Color.Argb(255,220,220,220));
					BackWA.SetBackgroundColor(Color.White);
					RightSeparator.SetBackgroundColor(Color.White);
					LeftSeparator.SetBackgroundColor(Color.Argb(255,76,156,206));
					ButtonFB.SetBackgroundResource(Resource.Drawable.RoundedViewLightGray);
					ButtonWA.SetBackgroundResource(Resource.Drawable.RoundedViewWhite);
					isFacebook = false;
					 
				}

			};
			Console.WriteLine ("4");
			// ******************** FB Part ***************

			FBView = FindViewById<RelativeLayout> (Resource.Id.FBView);
			CorniceListFB = FindViewById<LinearLayout> (Resource.Id.CorniceListFB);
			CorniceListFB.Visibility = ViewStates.Gone;
			NameFB = FindViewById<EditText>(Resource.Id.NomeFB);
			NameFB.Typeface = RobotoR;

			WAButtonSend = FindViewById<ImageView> (Resource.Id.SendButtonFB);

			noResult = FindViewById<TextView> (Resource.Id.NoResultFB);
			noResult.Visibility = ViewStates.Gone;

			NameFB.Click += delegate {
				if(TabellaOpen){
					Console.WriteLine("EditText");
					if(ListPeople.Count == 0)
						noResult.Visibility = ViewStates.Gone;
					else
						CorniceListFB.RemoveAllViews();
					CorniceListFB.Visibility = ViewStates.Gone;
					TabellaOpen = false;
				}
			};

			WAButtonSend.Click += delegate {

				if(!string.IsNullOrEmpty(NameFB.Text) && !string.IsNullOrWhiteSpace(NameFB.Text) && !TabellaOpen)
				{
					CorniceListFB.Visibility = ViewStates.Visible;

					ListPeople.Clear();

					RetriveFBPerson(NameFB.Text);

					//ListPeople.Add(new FbPerson("Mario","Rossi", "http://www.netwintec.com/wp-content/uploads/2014/03/full03-1024x640.jpg"));
					//ListPeople.Add(new FbPerson("Mario", "Bianchi", "http://www.netwintec.com/wp-content/uploads/2014/03/full03-1024x640.jpg"));
					//ListPeople.Add(new FbPerson("Mario", "aaa", "http://www.netwintec.com/wp-content/uploads/2014/03/full03-1024x640.jpg"));
					//ListPeople.Add(new FbPerson("Mario", "bbb", "http://www.netwintec.com/wp-content/uploads/2014/03/full03-1024x640.jpg"));
					//ListPeople.Add(new FbPerson("Mario", "ccc", "http://www.netwintec.com/wp-content/uploads/2014/03/full03-1024x640.jpg"));

					if(ListPeople.Count ==0){
						noResult.Visibility = ViewStates.Visible;
					}
					else{
						ListView ListFBName = new ListView(ApplicationContext);
						LinearLayout.LayoutParams ll = new LinearLayout.LayoutParams(0,0);
						/*if (ListPeople.Count>0 && ListPeople.Count < 4)
							ll = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MatchParent,PixelsToDp((42 * ListPeople.Count)+8));
						if(ListPeople.Count>=4)
							ll = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MatchParent,PixelsToDp(180));
						*/
						if (ListPeople.Count > 0)
						{
							ll = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MatchParent, LinearLayout.LayoutParams.WrapContent);
						}
						ll.SetMargins(PixelsToDp(2),PixelsToDp(2),PixelsToDp(2),PixelsToDp(2));

						ListFBName.LayoutParameters  = ll;

						//ListFBName.SetBackgroundColor(Android.Graphics.Color.Argb(255,230,230,230));
						ListFBName.SetBackgroundColor(Android.Graphics.Color.White);
						ListFBName.SetFooterDividersEnabled (false);
						ListFBName.SetHeaderDividersEnabled (false);
						ListFBName.Divider.SetAlpha (0);

						//mDrawerList.Adapter = new ArrayAdapter<string> (this, Resource.Layout.item_menu, Section);
						adapter = new ListFbPersonAdapter(this,ListPeople);

						ListFBName.SetAdapter(adapter);
						ListFBName.ItemClick+=ChatListItemClick;
						CorniceListFB.AddView(ListFBName);
					}
					TabellaOpen = true;

				}

			};

			Console.WriteLine ("5");
			// ******************** WA Part ***************

			WAView = FindViewById<RelativeLayout> (Resource.Id.WAView);
			WAView.Visibility = ViewStates.Invisible;

			Spinner spinner = FindViewById<Spinner> (Resource.Id.spinner1);

			//spinner.ItemSelected += new EventHandler<AdapterView.ItemSelectedEventArgs> (spinner_ItemSelected);
			var adapter2 = ArrayAdapter.CreateFromResource (
				this, Resource.Array.prefissi_Array, Android.Resource.Layout.SimpleSpinnerItem);

			adapter2.SetDropDownViewResource (Android.Resource.Layout.SimpleSpinnerDropDownItem);
			spinner.Adapter = adapter2;

			WAButtonSend = FindViewById<ImageView> (Resource.Id.SendButtonWA);
			NumberWA = FindViewById<EditText>(Resource.Id.NumeroWA);
			SearchContact = FindViewById<Button> (Resource.Id.ContattiButton);
			NumberWA.Typeface = RobotoR;

			SearchContact.Click += delegate {

				Intent pickcontact= new Intent(Intent.ActionPick,ContactsContract.Contacts.ContentUri);
				StartActivityForResult(pickcontact,3);

			};

			WAButtonSend.Click += delegate {

				Console.WriteLine(spinner.SelectedItem.ToString());

				if(!string.IsNullOrEmpty(NumberWA.Text) && !string.IsNullOrWhiteSpace(NumberWA.Text))
				{
					var intent = new Intent (this, typeof(ChatPageActivity));
					intent.PutExtra ("isPreliminary",true);
					intent.PutExtra ("isFirstMessage",true);
					intent.PutExtra ("name",NumberWA.Text);
					intent.PutExtra ("Number",spinner.SelectedItem.ToString()+NumberWA.Text);
					this.StartActivity (intent);
				}

			};
			Console.WriteLine ("6");
		}

		public override void OnBackPressed ()
		{
			var intent = new Intent (this, typeof(HomePageActivity));
			this.StartActivity (intent);
			Finish();
		}

		protected override int getLayoutResource() {
			return Resource.Layout.AddChatPage;
		}

		public void ChatListItemClick ( object sender , AdapterView.ItemClickEventArgs item ){
			selectItem(item.Position);

		}

		public void selectItem(int position) {

			//chatList.SetItemChecked(position,true);
			var intent = new Intent (this, typeof(ChatPageActivity));
			intent.PutExtra ("isPreliminary",true);
			intent.PutExtra ("isFirstMessage",true);
			intent.PutExtra ("name",ListPeople[position].Nome);
			intent.PutExtra ("FacebookId",ListPeople[position].ID);
			this.StartActivity (intent);
			Finish();

		}

		private int PixelsToDp(int pixels)
		{
			return (int)Android.Util.TypedValue.ApplyDimension(Android.Util.ComplexUnitType.Dip, pixels, Resources.DisplayMetrics);
		}

		protected override void OnActivityResult(int requestCode, Result resultCode, Intent data)
		{
			
			//base.OnActivityResult(requestCode, resultCode, data);
			Console.WriteLine("1");
			string pno="";
			Console.WriteLine("2a");

			bool error = false;
			Android.Database.ICursor cursor=null;

			try{
				Console.WriteLine("2b");
				cursor = ManagedQuery (data.Data, null, null, null, null);
				Console.WriteLine("3");
			}catch(Exception e){
				error = true;
			}

			if(!error){
				
				while (cursor.MoveToNext ()) {
					Console.WriteLine ("4");
					string contactId = cursor.GetString (cursor.GetColumnIndex (ContactsContract.Contacts.InterfaceConsts.Id));
					Console.WriteLine ("5");
					var Name = cursor.GetString (cursor.GetColumnIndexOrThrow (ContactsContract.Contacts.InterfaceConsts.DisplayName));
					Console.WriteLine ("6");
					string hasPhone = cursor.GetString (cursor.GetColumnIndex (ContactsContract.Contacts.InterfaceConsts.HasPhoneNumber));
					Console.WriteLine ("7:" + hasPhone);
					if (hasPhone.Equals ("1"))
						hasPhone = "true";
					else
						hasPhone = "false";
					int i = 0;
					Console.WriteLine ("8");
					if (bool.Parse (hasPhone)) {
						String ret = null;
						String selection = /*ContactsContract.CommonDataKinds.Phone.SearchDisplayNameKey+*/"display_name like'%" + Name +"%'";
						String[] projection = new String[] { ContactsContract.CommonDataKinds.Phone.Number};
						var c = ContentResolver.Query (ContactsContract.CommonDataKinds.Phone.ContentUri,
							projection, selection, null, null);
						if (c.MoveToFirst()) {
							ret = c.GetString(0);
						}
						c.Close();
						if (ret == null) {
							return;
						}

						Console.WriteLine ("NUMBER:" + Name + "=" + ret);

						if (ret.Contains ("+39") || ret.Contains ("+34") || ret.Contains ("+33") || ret.Contains ("+49") || ret.Contains ("+44") || ret.Contains ("+41") || ret.Contains ("+32") || ret.Contains ("+31"))
							ret = ret.Substring (3);
						if (ret.Contains ("+1") )
							ret = ret.Substring (2);
						if (ret.Contains ("+420") || ret.Contains ("+351") || ret.Contains ("+353") || ret.Contains ("+385") || ret.Contains ("+386"))
							ret = ret.Substring (4);
						Console.WriteLine (ret);
						ret = ret.Replace (" ", "");
						NumberWA.Text = ret;
					}
				}
			}
		
		}

		// ***************** GET FB PERSON FROM SEARCH RESULT ******************

		public void RetriveFBPerson(string text){

			ISharedPreferences prefs = MainActivity.Instance.prefs;
			string tokenFB = prefs.GetString("TokenFB", "");

			if (tokenFB != "")
			{
				var client = new RestClient("https://graph.facebook.com/");
				//DISTRIBUTION
				//var client = new RestClient("http://api.netwintec.com:3000"/");

				string name = text.Replace(" ", "%20");

				Console.WriteLine("https://graph.facebook.com/v2.5/search?q=" + name + "&type=user&field=id,name,picture&access_token=" + tokenFB + "&limit=50&offset=0");

				var requestN4U = new RestRequest("v2.5/search?q=" + name + "&type=user&field=id,name,picture&access_token=" + tokenFB + "&limit=50&offset=0", Method.GET);


				IRestResponse response = client.Execute(requestN4U);

				Console.WriteLine("RESPONSE:" + (response.StatusCode == System.Net.HttpStatusCode.OK) + "|" + response.Content);
				if (response.StatusCode == System.Net.HttpStatusCode.OK)
				{
					Console.WriteLine(1);
					JsonValue json = JsonValue.Parse(response.Content);
					JsonValue value = json["data"];
					Console.WriteLine(2);
					foreach (JsonValue j in value)
					{
						Console.WriteLine(j["name"].ToString());
						string id = j["id"];
						string nome = j["name"];
						string picture = "https://graph.facebook.com/v2.5/" + id + "/picture?type=normal";

						FbPerson c = new FbPerson(id, nome, picture);
						ListPeople.Add(c);

					}


					//Console.WriteLine (response.Content);

				}

			}
			else {

				//var client = new RestClient("https://graph.facebook.com/");
				//DISTRIBUTION
				var client = new RestClient("http://api.netwintec.com:3000/");

				string name = text.Replace(" ", "%20");

				//Console.WriteLine("https://graph.facebook.com/v2.5/search?q=" + name + "&type=user&field=id,name,picture&access_token=" + tokenFB + "&limit=50&offset=0");

				var requestN4U = new RestRequest("api/facebookSearch/"+name, Method.GET);
				requestN4U.AddHeader("content-type", "application/json");

				IRestResponse response = client.Execute(requestN4U);

				Console.WriteLine("RESPONSE:" + (response.StatusCode == System.Net.HttpStatusCode.OK) + "|" + response.Content);
				if (response.StatusCode == System.Net.HttpStatusCode.OK)
				{

					JsonValue json = JsonValue.Parse(response.Content);
					//JsonValue value = json["data"];

					foreach (JsonValue j in json)
					{

						string id = j["id"];
						string nome = j["name"];
						string picture = "https://graph.facebook.com/v2.5/" + id + "/picture?type=normal";

						FbPerson c = new FbPerson(id, nome, picture);
						ListPeople.Add(c);

					}


					//Console.WriteLine (response.Content);

				}

			}

		}

	}
}

