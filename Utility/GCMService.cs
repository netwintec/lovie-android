﻿using System.Text;
using System;
using System.Threading;
using Android.App;
using Android.Content;
using Android.Util;
using Gcm.Client;
using Badge.Plugin;

[assembly: UsesPermission (Android.Manifest.Permission.ReceiveBootCompleted)]

namespace Lovie
{
	//You must subclass this!
	[BroadcastReceiver(Permission=Constants.PERMISSION_GCM_INTENTS)]
	[IntentFilter(new string[] { Constants.INTENT_FROM_GCM_MESSAGE }, Categories = new string[] { "@PACKAGE_NAME@" })]
	[IntentFilter(new string[] { Constants.INTENT_FROM_GCM_REGISTRATION_CALLBACK }, Categories = new string[] { "@PACKAGE_NAME@" })]
	[IntentFilter(new string[] { Constants.INTENT_FROM_GCM_LIBRARY_RETRY }, Categories = new string[] { "@PACKAGE_NAME@" })]

	public class GcmBroadcastReceiver : GcmBroadcastReceiverBase<PushHandlerService>
	{
		//IMPORTANT: Change this to your own Sender ID!
		//The SENDER_ID is your Google API Console App Project ID.
		//  Be sure to get the right Project ID from your Google APIs Console.  It's not the named project ID that appears in the Overview,
		//  but instead the numeric project id in the url: eg: https://code.google.com/apis/console/?pli=1#project:785671162406:overview
		//  where 785671162406 is the project id, which is the SENDER_ID to use!
		public static string[] SENDER_IDS = new string[] {"762855674567"};

		public const string TAG = "PushSharp-GCM";
	}

	[Service] //Must use the service tag
	public class PushHandlerService : GcmServiceBase
	{
		public PushHandlerService() : base(GcmBroadcastReceiver.SENDER_IDS) { }

		const string TAG = "GCM-SAMPLE";

		protected override void OnRegistered (Context context, string registrationId)
		{
			Log.Verbose(TAG, "GCM Registered: " + registrationId);
			//Eg: Send back to the server
			//	var result = wc.UploadString("http://your.server.com/api/register/", "POST", 
			//		"{ 'registrationId' : '" + registrationId + "' }");

			MainActivity.Instance.PushToken = registrationId;

			//createNotification("GCM Registered...", "The device has been Registered, Tap to View!");
		}

		protected override void OnUnRegistered (Context context, string registrationId)
		{
			Log.Verbose(TAG, "GCM Unregistered: " + registrationId);
			//Remove from the web service
			//	var wc = new WebClient();
			//	var result = wc.UploadString("http://your.server.com/api/unregister/", "POST",
			//		"{ 'registrationId' : '" + lastRegistrationId + "' }");

			//createNotification("GCM Unregistered...", "The device has been unregistered, Tap to View!");
		}

		protected override void OnMessage (Context context, Intent intent)
		{
			Log.Info(TAG, "GCM Message Received!");

			var msg = new StringBuilder();  

			if (intent != null && intent.Extras != null)
			{
				foreach (var key in intent.Extras.KeySet())
					msg.AppendLine(key + "=" + intent.Extras.Get(key).ToString());
			}

			Console.WriteLine ("message:\n"+msg);

			bool reveal = false;
			string Message = "";
			try{
				Message = intent.Extras.Get("message").ToString();
			}catch(Exception e){
				reveal = true;
			}

			string Conv_ID = intent.Extras.Get("conversation_id").ToString();
			string name = intent.Extras.Get("sender_name").ToString();

			Console.WriteLine ("1:"+Message+"|"+Conv_ID);
			bool close2;
			try{
				close2 = MainActivity.Instance.IsFinishing;
			}catch(Exception e){
				close2 = true;
			}

			//MainActivity.Instance.Notification

			Console.WriteLine ("2:"+close2);
			if(!reveal)
				createNotification("Hai un nuovo messaggio da "+name,close2,Message,Conv_ID,name);
			else
				createNotification("Un utente si è appena rivelato",close2,Message,Conv_ID,name);
			
		}

		protected override bool OnRecoverableError (Context context, string errorId)
		{
			Log.Warn(TAG, "Recoverable Error: " + errorId);
			return base.OnRecoverableError (context, errorId);
		}

		protected override void OnError (Context context, string errorId)
		{
			Log.Error(TAG, "GCM Error: " + errorId);
		}

		void createNotification(string title,bool close2,string message,string Conv_ID,string name)
		{
			var prefs = MainActivity.Instance.prefs;
			var prefEditor = prefs.Edit();
			int Badge = prefs.GetInt("Badge", 0);
			prefEditor.PutInt("Badge", Badge+1);
			prefEditor.Commit();

			CrossBadge.Current.SetBadge(Badge + 1);

			Console.WriteLine ("cN:"+message+"|"+Conv_ID);

			var manager =
				(NotificationManager)this.ApplicationContext.GetSystemService(Context.NotificationService);

			Intent intent=null;

			if (close2) {

				var intent2 = ApplicationContext.PackageManager.GetLaunchIntentForPackage(this.ApplicationContext.PackageName);
				intent = new Intent (this, typeof(SplashScreen));
				intent.PutExtra ("name",name);
				intent.PutExtra ("key",Conv_ID);
				intent.PutExtra ("Notification",true);

			} else {

				if (MainActivity.Instance.isHome) {

					if (MainActivity.Instance.isStopped) {
						intent = new Intent (this, typeof(ChatPageActivity));
						intent.PutExtra ("key",Conv_ID);
						intent.PutExtra ("name",name);
						intent.PutExtra ("isPreliminary",false);
						intent.PutExtra ("isFirstMessage",false);

						HomePageActivity.Instance.ChangeDataNotification ();
					} else {
						HomePageActivity.Instance.ChangeDataNotification ();
						return;
					}

				}

				if (MainActivity.Instance.isChat) {

					if (Conv_ID == MainActivity.Instance.ChatKey) {

						Console.WriteLine ("PROVAAA:" + MainActivity.Instance.isStopped);

						if (MainActivity.Instance.isStopped) {
							
							var builder2 = new Notification.Builder(this.ApplicationContext)
								.SetDefaults(NotificationDefaults.Sound | NotificationDefaults.Vibrate)
								.SetSmallIcon(Resource.Drawable.logo_lovie)
								.SetContentTitle("Lovie")
								.SetContentText (title)
								.SetAutoCancel(true);



							manager.Notify(1, builder2.Build());
							ChatPageActivity.Instance.removeNot = true;
							if(!string.IsNullOrWhiteSpace(message))
								ChatPageActivity.Instance.AddNewMessage (message);

							return;
							
						} else {
							if(!string.IsNullOrWhiteSpace(message))
								ChatPageActivity.Instance.AddNewMessage (message);
							return;
						}

					} else {
						intent = new Intent (this, typeof(ChatPageActivity));
						intent.PutExtra ("key",Conv_ID);
						intent.PutExtra ("name",name);
						intent.PutExtra ("isPreliminary",false);
						intent.PutExtra ("isFirstMessage",false);
					}

				}

				if (MainActivity.Instance.isNothing) {

					intent = new Intent (this, typeof(ChatPageActivity));
					intent.PutExtra ("key",Conv_ID);
					intent.PutExtra ("name",name);
					intent.PutExtra ("isPreliminary",false);
					intent.PutExtra ("isFirstMessage",false);
					intent.AddFlags (ActivityFlags.ClearTop);

					
				}
					

			}

			var pendingIntent = PendingIntent.GetActivity(this.ApplicationContext, 0, intent, PendingIntentFlags.UpdateCurrent);

			var builder = new Notification.Builder(this.ApplicationContext)
				.SetDefaults(NotificationDefaults.Sound | NotificationDefaults.Vibrate)
			    .SetSmallIcon(Resource.Drawable.logo_lovie)
				.SetContentTitle("Lovie")
				.SetContentText (title)
				.SetContentIntent(pendingIntent)
				.SetAutoCancel(true);




			manager.Notify(1, builder.Build());

		}
	}
}

