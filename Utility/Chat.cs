﻿using System;
using System.Collections.Generic;
using System.Text;

using Android.Graphics;


namespace Lovie
{
    public class Chat
    {

        public string Key,Nome, Messaggio;
		public DateTime DataLastMessage;
		public string profileImage, FacebookId,Number;
		public Bitmap profileImageBitmap;
		public bool newMessage,isPreliminary;

		public Chat(string key,string nome, string messaggio, DateTime dataLastMessage,bool somethingNew, string image, bool preliminary,string FBId,string n)
        {
			Key = key;
            Nome = nome;
            Messaggio = messaggio;
            DataLastMessage = dataLastMessage;
			newMessage = somethingNew;
            profileImage = image;
			isPreliminary = preliminary;
			FacebookId = FBId;
			Number = n;
			profileImageBitmap = null;

        }

    }

}
