﻿using System;
using System.Collections.Generic;
using System.Text;

using Android.Graphics;

namespace Lovie
{
	public class FbPerson
    {

        public string ID,Nome,Image_Url;
		public Bitmap profileImageBitmap;

        public FbPerson(string id, string nome, string image)
        {

            ID = id;
            Nome = nome;
            Image_Url = image;
			profileImageBitmap = null;

        }

    }
}
