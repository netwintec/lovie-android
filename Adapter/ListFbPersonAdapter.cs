﻿using System;

using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Threading.Tasks;

using Android.App;
using Android.Content;
using Android.Graphics;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using ImageViews.Rounded;
using Android.Webkit;

namespace Lovie
{
	public class ListFbPersonAdapter: BaseAdapter
	{
		AddChatPageActivity super;

		public List<FbPerson> listChat;

		public ListFbPersonAdapter(AddChatPageActivity context,List<FbPerson> lc) : base()
		{
			super = context;
			listChat = lc;
		}

		public override int Count
		{
			get { return listChat.Count; }
		}

		public override Java.Lang.Object GetItem(int position)
		{
			return position;
		}

		public override long GetItemId(int position)
		{
			return position;
		}

		public override View GetView(int position, View convertView, ViewGroup parent)
		{
			//Get our object for this position
			//var item = items[position];  
			Typeface RobotoR = Typeface.CreateFromAsset(super.Assets,"fonts/Roboto_Regular.ttf");

			View view = convertView;

			LayoutInflater inflater = ((Activity) super).LayoutInflater;

			view = inflater.Inflate(Resource.Layout.FbPersonLayout, parent, false);

			FbPerson Item = this.listChat[position];
			if (Item.profileImageBitmap == null) {
				caricaImmagineAsync (Item.Image_Url, view.FindViewById<RoundedImageView> (Resource.Id.Image),Item);
			} else {
				view.FindViewById<RoundedImageView> (Resource.Id.Image).SetImageBitmap (Item.profileImageBitmap);
			}
			view.FindViewById<TextView> (Resource.Id.Nome).Text = Item.Nome;
		
			return view;
		}

		public FbPerson GetItemAtPosition(int position)
		{
			return listChat[position];
		}


		async void caricaImmagineAsync(String uri,RoundedImageView immagine_view,FbPerson item){
			WebClient webClient = new WebClient ();
			byte[] bytes = null;
			try{
				bytes = await webClient.DownloadDataTaskAsync(uri);
			}
			catch(TaskCanceledException){
				Console.WriteLine ("Task Canceled!**************************************");
				return;
			}
			catch(Exception e){
				Console.WriteLine (e.ToString());
				return;
			}

			//if (immagine_view.IsInLayout) {

			Bitmap immagine = await BitmapFactory.DecodeByteArrayAsync (bytes, 0, bytes.Length);

			immagine_view.SetImageBitmap (immagine);
			item.profileImageBitmap = immagine;

			Console.WriteLine ("Immagine caricata!");

		}
	}

}

