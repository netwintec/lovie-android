﻿using System;

using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Threading.Tasks;

using Android.App;
using Android.Content;
using Android.Graphics;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using ImageViews.Rounded;
using Android.Webkit;

namespace Lovie
{
	public class ListChatAdapter: BaseAdapter
	{
		ChatPageActivity super;

		public List<Message> listChat;

		public ListChatAdapter(ChatPageActivity context,List<Message> lc) : base()
		{
			super = context;
			listChat = lc;
		}

		public override int Count
		{
			get { return listChat.Count; }
		}

		public override Java.Lang.Object GetItem(int position)
		{
			return position;
		}

		public override long GetItemId(int position)
		{
			return position;
		}

		public override View GetView(int position, View convertView, ViewGroup parent)
		{
			//Get our object for this position
			//var item = items[position];  
			Typeface RobotoR = Typeface.CreateFromAsset(super.Assets,"fonts/Roboto_Regular.ttf");
			//Typeface RobotoB = Typeface.CreateFromAsset(super.Assets,"fonts/Roboto_Bold.ttf");


			View view = convertView;

			LayoutInflater inflater = ((Activity) super).LayoutInflater;

			Message Item = this.listChat[position];

			if (Item.isMine) {

				view = inflater.Inflate(Resource.Layout.MessageSendLayout, parent, false);

				view.FindViewById<TextView>(Resource.Id.MessageText).Text=Item.Text;
				view.FindViewById<TextView>(Resource.Id.MessageText).Typeface = RobotoR;

			}
			else
			{
				view = inflater.Inflate(Resource.Layout.MessageReceiveLayout, parent, false);

				view.FindViewById<TextView>(Resource.Id.MessageText).Text=Item.Text;
				view.FindViewById<TextView>(Resource.Id.MessageText).Typeface = RobotoR;

			}

			/*
			view = inflater.Inflate(Resource.Layout.ChatLayout, parent, false);

			Chat Item = this.listChat[position];
			if (Item.profileImageBitmap == null) {
				caricaImmagineAsync (Item.profileImage, view.FindViewById<RoundedImageView> (Resource.Id.Image),Item);
			} else {
				view.FindViewById<RoundedImageView> (Resource.Id.Image).SetImageBitmap (Item.profileImageBitmap);
			}
			view.FindViewById<TextView>(Resource.Id.Nome).Text=Item.Nome+" "+Item.Cognome;
			view.FindViewById<TextView>(Resource.Id.Messaggio).Text=Item.Messaggio;

			DateTime time = DateTime.Now.ToLocalTime ();
			if (Item.DataLastMessage.Month == time.Month && Item.DataLastMessage.Year == time.Year && Item.DataLastMessage.Day == time.Day) {
				view.FindViewById<TextView> (Resource.Id.Data).Text = Item.DataLastMessage.ToString ("HH':'mm");
			} else {
				view.FindViewById<TextView> (Resource.Id.Data).Text = Item.DataLastMessage.ToString ("dd'/'MM");
			}

			view.FindViewById<TextView> (Resource.Id.Data).Typeface = RobotoR;

			if (Item.DataLastMessage.CompareTo (Item.DataLastAccess) == 1) {

				view.FindViewById<View> (Resource.Id.Circle).Visibility = ViewStates.Visible;			
				view.FindViewById<TextView> (Resource.Id.Nome).Typeface = RobotoB;
				view.FindViewById<TextView> (Resource.Id.Messaggio).Typeface = RobotoB;
				view.FindViewById<TextView> (Resource.Id.Messaggio).TextSize = 12;

			} else {

				view.FindViewById<View> (Resource.Id.Circle).Visibility = ViewStates.Invisible;
				view.FindViewById<TextView> (Resource.Id.Nome).Typeface = RobotoR;
				view.FindViewById<TextView> (Resource.Id.Messaggio).Typeface = RobotoR;

			}*/


			return view;
		}

		public Message GetItemAtPosition(int position)
		{
			return listChat[position];
		}
	}
}

