﻿using System;

using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Threading.Tasks;

using Android.App;
using Android.Content;
using Android.Graphics;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using ImageViews.Rounded;
using Android.Webkit;

namespace Lovie
{
	public class HomeChatAdapter : BaseAdapter
	{
		HomePageActivity super;

		public List<Chat> listChat;

		public HomeChatAdapter(HomePageActivity context,List<Chat> lc) : base()
		{
			super = context;
			listChat = lc;
		}

		public override int Count
		{
			get { return listChat.Count; }
		}

		public override Java.Lang.Object GetItem(int position)
		{
			return position;
		}

		public override long GetItemId(int position)
		{
			return position;
		}

		public override View GetView(int position, View convertView, ViewGroup parent)
		{
			//Get our object for this position
			//var item = items[position];  
			Typeface RobotoR = Typeface.CreateFromAsset(super.Assets,"fonts/Roboto_Regular.ttf");
			Typeface RobotoB = Typeface.CreateFromAsset(super.Assets,"fonts/Roboto_Bold.ttf");

			View view = convertView;

			LayoutInflater inflater = ((Activity) super).LayoutInflater;

			view = inflater.Inflate(Resource.Layout.ChatLayout, parent, false);

			Chat Item = this.listChat[position];
			if (Item.profileImage.CompareTo ("standard") != 0) {
				if (Item.profileImageBitmap == null) {
					caricaImmagineAsync (Item.profileImage, view.FindViewById<RoundedImageView> (Resource.Id.Image), Item);
				} else {
					view.FindViewById<RoundedImageView> (Resource.Id.Image).SetImageBitmap (Item.profileImageBitmap);
				}
			} else {
				//view.FindViewById<RoundedImageView> (Resource.Id.Image).SetImageResource (Resource.Drawable.profilePicture);
			}
			view.FindViewById<TextView>(Resource.Id.Nome).Text=Item.Nome;
			view.FindViewById<TextView>(Resource.Id.Messaggio).Text=Item.Messaggio;

			DateTime time = DateTime.Now.ToLocalTime ();
			Console.WriteLine ("Time:"+Item.DataLastMessage.Month+"|"+time.Month+"|"+Item.DataLastMessage.Year+"|"+time.Year+"|"+Item.DataLastMessage.Day+"|"+time.Day);
			if (Item.DataLastMessage.Month == time.Month && Item.DataLastMessage.Year == time.Year && Item.DataLastMessage.Day == time.Day) {
				view.FindViewById<TextView> (Resource.Id.Data).Text = Item.DataLastMessage.ToString ("HH':'mm");
			} else {
				view.FindViewById<TextView> (Resource.Id.Data).Text = Item.DataLastMessage.ToString ("dd'/'MM");
			}

			view.FindViewById<TextView> (Resource.Id.Data).Typeface = RobotoR;
				
			if (Item.newMessage) {
				
				view.FindViewById<View> (Resource.Id.Circle).Visibility = ViewStates.Visible;			
				view.FindViewById<TextView> (Resource.Id.Nome).Typeface = RobotoB;
				view.FindViewById<TextView> (Resource.Id.Messaggio).Typeface = RobotoB;
				view.FindViewById<TextView> (Resource.Id.Messaggio).TextSize = 12;

			} else {
				
				view.FindViewById<View> (Resource.Id.Circle).Visibility = ViewStates.Invisible;
				view.FindViewById<TextView> (Resource.Id.Nome).Typeface = RobotoR;
				view.FindViewById<TextView> (Resource.Id.Messaggio).Typeface = RobotoR;

			}


			return view;
		}

		public Chat GetItemAtPosition(int position)
		{
			return listChat[position];
		}


		async void caricaImmagineAsync(String uri,RoundedImageView immagine_view,Chat item){
			WebClient webClient = new WebClient ();
			byte[] bytes = null;
			try{
				bytes = await webClient.DownloadDataTaskAsync(uri);
			}
			catch(TaskCanceledException){
				Console.WriteLine ("Task Canceled!**************************************");
				return;
			}
			catch(Exception e){
				Console.WriteLine (e.ToString());
				return;
			}

			//if (immagine_view.IsInLayout) {

			Bitmap immagine = await BitmapFactory.DecodeByteArrayAsync (bytes, 0, bytes.Length);

			immagine_view.SetImageBitmap (immagine);
			item.profileImageBitmap = immagine;

			Console.WriteLine ("Immagine caricata!");

		}
	}
		
}

