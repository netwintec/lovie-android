﻿using System;
using Android.App;
using Android.Content.PM;
using Android.OS;
using Android.Content;
using System.Threading;
using System.Threading.Tasks;


namespace Lovie
{
	[Activity( MainLauncher = true, NoHistory = true, Theme = "@style/Theme.Splash2", ScreenOrientation = ScreenOrientation.Portrait)]
	public class SplashScreen : Activity
	{
		bool notification;
		string name,conv_id;

		protected override void OnCreate(Bundle bundle)
		{
			base.OnCreate(bundle);


			notification = Intent.GetBooleanExtra ("Notification", false);
			name = Intent.GetStringExtra ("name");
			conv_id = Intent.GetStringExtra ("key");

			SetContentView (Resource.Layout.SplashScreen);
			ThreadPool.QueueUserWorkItem(o=> LoadActivity ());
		}
		public void LoadActivity(){
			Thread.Sleep (1000);
			RunOnUiThread (() => {
				var intent = new Intent(this, typeof(MainActivity));

				if(notification){

					intent.PutExtra ("name",name);
					intent.PutExtra ("key",conv_id);
					intent.PutExtra ("Notification",true);

				}

				StartActivity(intent);
				Finish();
			});
		}
	}
}

