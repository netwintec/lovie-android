﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Json;
using Java.Util;
using System.Threading;
using System.Threading.Tasks;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Content.PM;
using Android.Views.Animations;
using Android.Animation;

using RestSharp;

namespace Lovie
{
	[Activity (Label = "HomePageActivity",Theme = "@style/AppTheme",ScreenOrientation = ScreenOrientation.Portrait)]			
	public class HomePageActivity : BaseActivity
	{

		ListView chatList;
		HomeChatAdapter adapter;
		List<Chat> listChatDati = new List<Chat> ();
		ImageView menuIcon;
		ImageView backIcon;

		ImageView loadImage;
		public ProgressBar progress;
		Animation anim;

		bool notification;
		string name,conv_id;
		bool change = false;

		public static HomePageActivity Instance {get;private set;}

		protected override void OnCreate (Bundle savedInstanceState)
		{
			base.OnCreate (savedInstanceState);

			HomePageActivity.Instance = this;

			menuIcon = (ImageView) getToolbar().FindViewById(Resource.Id.menu_icon);
			backIcon = (ImageView) getToolbar().FindViewById(Resource.Id.back_icon);

			setActionBarIcon(Resource.Drawable.logo_lovie,Resource.Drawable.back);

			menuIcon.Visibility = ViewStates.Invisible;
			backIcon.Visibility = ViewStates.Invisible;

			MainActivity.Instance.isHome = true;
			MainActivity.Instance.isChat = false;
			MainActivity.Instance.ChatKey = "";
			MainActivity.Instance.isNothing = false;
			Console.WriteLine ("h1:" + MainActivity.Instance.isStopped);
			MainActivity.Instance.isStopped = false;
			Console.WriteLine ("h2:" + MainActivity.Instance.isStopped);

			notification = Intent.GetBooleanExtra ("Notification", false);
			name = Intent.GetStringExtra ("name");
			conv_id = Intent.GetStringExtra ("key");

			if (notification) {
				
				var intent = new Intent (this, typeof(ChatPageActivity));
				intent.PutExtra ("key", conv_id);
				intent.PutExtra ("name", name);
				intent.PutExtra ("isPreliminary", false);
				intent.PutExtra ("isFirstMessage", false);
				this.StartActivity (intent);
				Finish ();

			} else {
				RetriveData ();

				//loadImage = FindViewById<ImageView> (Resource.Id.loadImage);
				progress = FindViewById<ProgressBar>(Resource.Id.progressBar1);

				/*var a = ObjectAnimator.OfFloat (svgView,"percentage",0.0f,1.0f);
				a.SetDuration (1000);
				a.RepeatCount = int.MaxValue;
				a.SetInterpolator (new LinearInterpolator());
				a.Start();*/

				FindViewById<LinearLayout> (Resource.Id.sopra).Visibility = ViewStates.Visible;
				chatList = FindViewById<ListView> (Resource.Id.chatList);
				chatList.SetFooterDividersEnabled (false);
				chatList.SetHeaderDividersEnabled (false);
				chatList.Divider.SetAlpha (0);
				chatList.ItemClick += ChatListItemClick;


				//mDrawerList.Adapter = new ArrayAdapter<string> (this, Resource.Layout.item_menu, Section);
				adapter = new HomeChatAdapter (this, listChatDati);

				chatList.SetAdapter (adapter);

				LinearLayout Profile = FindViewById<LinearLayout> (Resource.Id.Profile);
				Profile.Click += delegate {
					var intent = new Intent (this, typeof(ProfilePageActivity));
					this.StartActivity (intent);
					Finish ();
				};

				LinearLayout AddChat = FindViewById<LinearLayout> (Resource.Id.AddChat);
				AddChat.Click += delegate {
					Console.WriteLine ("AAAAAAAAAAAAAAAAAAAAAAAA");
					var intent = new Intent (this, typeof(AddChatPageActivity));
					this.StartActivity (intent);
					Finish ();
				};

			}
			// Create your application here
		}

		public override void OnBackPressed ()
		{

		}

		protected override int getLayoutResource() {
			return Resource.Layout.HomePage;
		}

		public void ChatListItemClick ( object sender , AdapterView.ItemClickEventArgs item ){
			selectItem(item.Position);
		}

		public void selectItem(int position) {

			chatList.SetItemChecked(position,true);
			//FindViewById<LinearLayout> (Resource.Id.sopra).Visibility = ViewStates.Visible;

			/*var a = ObjectAnimator.OfFloat (svgView,"percentage",0.0f,1.0f);
			a.SetDuration (2000);
			a.RepeatCount = int.MaxValue;
			a.SetInterpolator (new LinearInterpolator());
			a.Start();*/


			//anim = AnimationUtils.LoadAnimation (this, Resource.Animation.RotateAnimation);
			//loadImage.StartAnimation (anim);
			//FindViewById<RelativeLayout> (Resource.Id.sopra).Invalidate();
			Task.Factory.StartNew (() => {
				ChangePage (position);
			});

		}

		public void ChangePage(int position){


			var intent = new Intent (this, typeof(ChatPageActivity));
			intent.PutExtra ("key", listChatDati [position].Key);
			intent.PutExtra ("name", listChatDati [position].Nome);
			intent.PutExtra ("isPreliminary", listChatDati [position].isPreliminary);
			if (listChatDati [position].isPreliminary){
				if(listChatDati [position].FacebookId !="")
					intent.PutExtra ("FacebookId", listChatDati [position].FacebookId);
				if(listChatDati [position].Number !="")
					intent.PutExtra ("Number", listChatDati [position].Number);
			}
			intent.PutExtra ("isFirstMessage", false);
			RunOnUiThread (() => {
				this.StartActivity (intent);
				change = true;
				//Finish ();
			});

		}


		// ************** RETRIVE DATA FROM SERVER *******************

		public void RetriveData(){

			//var client = new RestClient("http://location.keepup.pro:3000/");
			//DISTRIBUTION
			var client = new RestClient("http://api.netwintec.com:3000/");

			ISharedPreferences prefs = MainActivity.Instance.prefs;
			string token = prefs.GetString ("TokenN4U", "");

			if (token == "") {

				// ERRORE
				return;

			}

			var requestN4U = new RestRequest("api/conversations/get_conversations/"+token, Method.GET);
			requestN4U.AddHeader("content-type", "application/json");


			client.ExecuteAsync(requestN4U,(s,e) =>{

				IRestResponse response = s;
				Console.WriteLine ("RESPONSE:" + response.Content + "|" + response.StatusCode);
				if (response.StatusCode == System.Net.HttpStatusCode.OK) {

					List <Chat> listAppoggio = new List<Chat> ();
					JsonValue json = JsonValue.Parse(response.Content);

					foreach (JsonValue j in json) {

						string key = j["_key"];
						Console.WriteLine("KEY:"+key);
						long date = j["lastUpdate"];

						Calendar mCalendar = new GregorianCalendar();
						Java.Util.TimeZone mTimeZone = mCalendar.TimeZone;
						int mGMTOffset = mTimeZone.RawOffset;
						Console.WriteLine("GMT:"+mGMTOffset);
						date += mGMTOffset;

						DateTime dateMessage = FromUnixTime (date);
						bool SomethingNew = j["isSomethingNew"];
						bool StartbyMe = j["isStartedByMe"];
						bool isPreliminary = j["isPreliminary"];
						string FacebookId = "";
						string Number = "";
						if (isPreliminary) {

							string idApp = j ["idUserTo"];
							if(idApp.Length >16)
								FacebookId = idApp.Substring (16);  
							else
								Number = idApp;
							Console.WriteLine (FacebookId+"|"+Number);
						}


						string picture = "standard";
						if (StartbyMe) {
							if (j ["absolutePictureTo"] != null) {
								picture = j ["absolutePictureTo"];
							}
						}
						else{
							Console.WriteLine("picture:"+j["absolutePictureFrom"]);
							if(j["absolutePictureFrom"]!=null){
								picture = j["absolutePictureFrom"];
							}
						}

						var messages = j["messages"];
						JsonValue jj = messages [0];
						string message = jj["message"];
						bool isMine = jj["isFromMe"];
						string name = "";
						if (isMine) {
							name = jj ["to"];
						} else {
							name = jj ["from"];
						}
							
						Chat c = new Chat (key,name,message,dateMessage,SomethingNew,picture,isPreliminary,FacebookId,Number);
						listAppoggio.Add (c);

					}

					RunOnUiThread(() => {
						for (int i = listAppoggio.Count - 1; i >= 0; i--) {

							listChatDati.Add (listAppoggio [i]);
							//Enumerable.Reverse (list);
						}
						//adapter.listChat.Add(listChatApp[i]);
						adapter.NotifyDataSetChanged();
						Console.WriteLine ("FINEEEEEE");


						FindViewById<LinearLayout> (Resource.Id.sopra).Visibility = ViewStates.Gone;
					});

				}
			});
		}

		// ************** RETRIVE DATA FROM SERVER AFTER NOTIFICATION *******************
		public void ChangeDataNotification(){

			List<Chat> listChatApp = new List<Chat> ();
			//var client = new RestClient("http://location.keepup.pro:3000/");
			//DISTRIBUTION
			var client = new RestClient("http://api.netwintec.com:3000/");

			ISharedPreferences prefs = MainActivity.Instance.prefs;
			string token = prefs.GetString ("TokenN4U", "");

			if (token == "") {

				// ERRORE
				return;

			}

			var requestN4U = new RestRequest("api/conversations/get_conversations/"+token, Method.GET);
			requestN4U.AddHeader("content-type", "application/json");


			client.ExecuteAsync(requestN4U,(s,e) => {

				if (s.StatusCode == System.Net.HttpStatusCode.OK) {

					JsonValue json = JsonValue.Parse(s.Content);

					foreach (JsonValue j in json) {

						string key = j["_key"];
						Console.WriteLine("KEY:"+key);
						long date = j["lastUpdate"];

						Calendar mCalendar = new GregorianCalendar();
						Java.Util.TimeZone mTimeZone = mCalendar.TimeZone;
						int mGMTOffset = mTimeZone.RawOffset;
						//Console.WriteLine("GMT:"+mGMTOffset);
						date += mGMTOffset;

						DateTime dateMessage = FromUnixTime (date);
						bool SomethingNew = j["isSomethingNew"];
						bool StartbyMe = j["isStartedByMe"];
						bool isPreliminary = j["isPreliminary"];
						string FacebookId = "";
						string Number = "";
						if (isPreliminary) {

							string idApp = j ["idUserTo"];
							if(idApp.Length >16)
								FacebookId = idApp.Substring (16);  
							else
								Number = idApp;
							Console.WriteLine (FacebookId+"|"+Number);
						}


						string picture = "standard";
						if (StartbyMe) {
							if (j ["absolutePictureTo"] != null) {
								picture = j ["absolutePictureTo"];
							}
						}
						else{
							Console.WriteLine("picture:"+j["absolutePictureFrom"]);
							if(j["absolutePictureFrom"]!=null){
								picture = j["absolutePictureFrom"];
							}
						}

						var messages = j["messages"];
						JsonValue jj = messages [0];
						string message = jj["message"];
						bool isMine = jj["isFromMe"];
						string name = "";
						if (isMine) {
							name = jj ["to"];
						} else {
							name = jj ["from"];
						}

						Chat c = new Chat (key,name,message,dateMessage,SomethingNew,picture,isPreliminary,FacebookId,Number);
						listChatApp.Add (c);

					}

					RunOnUiThread(() => {
						
						adapter.listChat.Clear();
						listChatDati.Clear();
						for(int i =listChatApp.Count-1 ; i>=0; i--)
							listChatDati.Add(listChatApp[i]);
						adapter.NotifyDataSetChanged();

					});
				}

			});

		}

		public DateTime FromUnixTime(long unixTime)
		{
			var epoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
			return epoch.AddMilliseconds(unixTime);
		}

		protected override void OnStop ()
		{
			
			Console.WriteLine("s1:"+MainActivity.Instance.isStopped);
			if (!change) {
				MainActivity.Instance.isStopped = true;
			} else {
				change = false;
			}
			Console.WriteLine("s2:"+MainActivity.Instance.isStopped);
			base.OnStop ();
		}

		protected override void OnRestart ()
		{
			Console.WriteLine("r1:"+MainActivity.Instance.isStopped);
			MainActivity.Instance.isStopped = false;
			Console.WriteLine("r2:"+MainActivity.Instance.isStopped);

			ChangeDataNotification();

			base.OnRestart ();
		}
			

	}
		
}

